﻿using RLGE.GameEngine;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RLGE.GameEngine.Entities;

namespace RLGE.Physics.Demo
{
    public class Game1 : GameBase
    {
        private EntityManager EntityManager { get; set; }

        public Game1()
        {
            //Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            //Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            //Graphics.ApplyChanges();
            //SetBorderlessWindow();

            Graphics.PreferredBackBufferWidth = 1366;
            Graphics.PreferredBackBufferHeight = 768;
            Graphics.ApplyChanges();

            EntityManager = new EntityManager(this);
            GameObjects.Add(EntityManager);

            var help = new Help(this);
            GameObjects.Add(help);
        }

        protected override void Initialize()
        {
            base.Initialize();

            IsMouseVisible = true;
        }

        protected override void Update()
        {
            foreach (var pressedKey in Controller)
            {
                switch (pressedKey)
                {
                    case Keys.Escape:
                        Exit();
                        break;
                    case Keys.Space:
                        Paused = !Paused;
                        break;
                }
            }
        }

        protected override void Draw()
        {
            if (Controller.ShowingContact || Controller.ShowingBoundsContact)
            {
                GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
                GraphicsDevice.BlendState = BlendState.AlphaBlend;
            }
        }
    }
}
