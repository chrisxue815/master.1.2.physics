﻿using Microsoft.Xna.Framework.Input;
using RLGE.GameEngine;
using RLGE.GameEngine.ConvexBodies;
using RLGE.GameEngine.Entities;
using Microsoft.Xna.Framework;
using RLGE.GameEngine.Renderers;
using RLGE.Physics.RigidBodyEngine;
using RLGE.Physics.Utilities;
using Rectangle = RLGE.GameEngine.ConvexBodies.Rectangle;

namespace RLGE.Physics.Demo
{
    public class EntityManager : GameObjectManager
    {
        private readonly Space space;
        private readonly GameObjectManager<ConvexBody> convexBodies;

        public EntityManager(GameBase game)
            : base(game)
        {
            space = new Space();
            space.GlobalAcceleration = new Vector3(0, -9.8f, 0);

            convexBodies = new GameObjectManager<ConvexBody>(Game);
            GameObjects.Add(convexBodies);

            game.Camera.LookAt(new Vector3(8, 8, 10), Vector3.Zero);

            var contactRenderer = new ContactRenderer(Game, space.SolverManager);
            GameObjects.Add(contactRenderer);
        }

        public override void Initialize()
        {
            base.Initialize();

            CreateBounds();
        }

        private void CreateBounds()
        {
            const float gridWidth = 1f;
            const int numGridX = 10;
            const int numGridZ = 10;
            const float groundWidth = numGridX * gridWidth;
            const float groundDepth = numGridZ * gridWidth;
            const float wallHeight = 10;
            const float spacing = 0.01f;
            const float doubleSpacing = spacing * 2;

            var ground = new GridLineGround(Game, numGridX, numGridZ, gridWidth);
            ground.RigidBody.Position = new Vector3();
            ground.Initialize();
            Add(ground);

            var ceiling = new Rectangle(Game, groundWidth, groundDepth);
            ceiling.RigidBody.Position = new Vector3(0, wallHeight + doubleSpacing, 0);
            ceiling.Initialize();
            Add(ceiling);

            var leftWall = new Rectangle(Game, wallHeight, groundDepth - doubleSpacing);
            leftWall.RigidBody.Position = new Vector3(-groundWidth / 2, wallHeight / 2 + spacing, 0);
            leftWall.RigidBody.Orientation = Quaternion.CreateFromYawPitchRoll(0, 0, -MathHelper.PiOver2);
            leftWall.Initialize();
            Add(leftWall);

            var rightWall = new Rectangle(Game, wallHeight, groundDepth - doubleSpacing);
            rightWall.RigidBody.Position = new Vector3(groundWidth / 2, wallHeight / 2 + spacing, 0);
            rightWall.RigidBody.Orientation = Quaternion.CreateFromYawPitchRoll(0, 0, MathHelper.PiOver2);
            rightWall.Initialize();
            Add(rightWall);

            var frontWall = new Rectangle(Game, groundWidth - doubleSpacing, wallHeight);
            frontWall.RigidBody.Position = new Vector3(0, wallHeight / 2 + spacing, groundDepth / 2);
            frontWall.RigidBody.Orientation = Quaternion.CreateFromYawPitchRoll(0, -MathHelper.PiOver2, 0);
            frontWall.Initialize();
            Add(frontWall);

            var backWall = new Rectangle(Game, groundWidth - doubleSpacing, wallHeight);
            backWall.RigidBody.Position = new Vector3(0, wallHeight / 2 + spacing, -groundDepth / 2);
            backWall.RigidBody.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.PiOver2, 0);
            backWall.Initialize();
            Add(backWall);
        }

        private void Add(ConvexBody convexBody)
        {
            convexBodies.Add(convexBody);
            space.Add(convexBody.Collider);
        }

        private static Vector3 RandomSize()
        {
            const float min = 1f;
            const float max = 2f;
            return RandomExt.NextVector3(min, max);
        }

        public override void Update()
        {
            if (Game.Controller[Keys.D1])
            {
                var rand = RandomExt.NextInt(3);
                var randomKey = (Keys) ((int) Keys.D2 + rand);
                Game.Controller[randomKey] = true;
            }
            else if (Game.Controller[Keys.D0])
            {
                for (var i = 0; i < 10; i++)
                {
                    var newConvexBody = new Cube(Game, RandomSize(), 1);
                    newConvexBody.RigidBody.Position = new Vector3(0, 5, 0);
                    newConvexBody.RigidBody.AngularVelocity = RandomExt.NextVector3();
                    newConvexBody.RigidBody.DiffuseColor = RandomExt.NextPosVector3();
                    newConvexBody.Initialize();
                    Add(newConvexBody);
                }
            }

            foreach (var pressedKey in Game.Controller)
            {
                ConvexBody newConvexBody = null;

                switch (pressedKey)
                {
                    case Keys.D2:
                        newConvexBody = new Cube(Game, RandomSize(), 1);
                        break;
                    case Keys.D3:
                        newConvexBody = new Sphere(Game, RandomSize(), 1);
                        break;
                    case Keys.D4:
                        newConvexBody = new Cylinder(Game, RandomSize(), 1);
                        break;
                    case Keys.C:
                        space.Clear();
                        convexBodies.Clear();
                        CreateBounds();
                        break;
                }

                if (newConvexBody != null)
                {
                    newConvexBody.RigidBody.Position = new Vector3(0, 5, 0);
                    newConvexBody.RigidBody.AngularVelocity = RandomExt.NextVector3();
                    newConvexBody.RigidBody.DiffuseColor = RandomExt.NextPosVector3();
                    newConvexBody.Initialize();
                    Add(newConvexBody);
                }
            }

            if (!Game.Paused) space.Update(Game.ElapsedTicks);

            var numBodies = string.Format("Convex bodies: {0}", convexBodies.Count);
            Game.Monitor.Add(numBodies);

            var hit = string.Format("Broad phase hit: {0}/{1} ({2:f1}%)", space.BroadPhaseHit, space.BroadPhaseCount, space.BroadPhaseHitRate * 100);
            Game.Monitor.Add(hit);

            hit = string.Format("Broad phase miss: {0}/{1} ({2:f1}%)", space.BroadPhaseMiss, space.BroadPhaseCount, space.BroadPhaseMissRate * 100);
            Game.Monitor.Add(hit);

            hit = string.Format("Narrow phase hit: {0}/{1} ({2:f1}%)", space.NarrowPhaseHit, space.NarrowPhaseCount, space.NarrowPhaseHitRate * 100);
            Game.Monitor.Add(hit);

            hit = string.Format("Narrow phase miss: {0}/{1} ({2:f1}%)", space.NarrowPhaseMiss, space.NarrowPhaseCount, space.NarrowPhaseMissRate * 100);
            Game.Monitor.Add(hit);

            base.Update();
        }
    }
}
