﻿using RLGE.Physics;

namespace RLGE.GameEngine
{
    public abstract class GameEntity : GameObject
    {
        public virtual Transform Transform { get; protected set; }

        protected GameEntity(GameBase game)
            : base(game)
        {
            Transform = new Transform();
        }
    }
}
