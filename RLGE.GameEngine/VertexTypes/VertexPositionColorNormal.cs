﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLGE.GameEngine.VertexTypes
{
    public struct VertexPositionColorNormal : IVertexType
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Color Color;

        public static readonly VertexDeclaration Declaration = new VertexDeclaration
        (
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float) * 3, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
            new VertexElement(sizeof(float) * 6, VertexElementFormat.Color, VertexElementUsage.Color, 0)
        );

        public VertexDeclaration VertexDeclaration { get { return Declaration; } }

        public VertexPositionColorNormal(Vector3 position, Color color, Vector3 normal)
        {
            Position = position;
            Color = color;
            Normal = normal;
        }

        public VertexPositionColorNormal(Vector3 position, Color color)
        {
            Position = position;
            Color = color;
            Normal = new Vector3();
        }

        public VertexPositionColorNormal(Vector3 position)
        {
            Position = position;
            Color = Color.Black;
            Normal = new Vector3();
        }

        public VertexPositionColorNormal(float x, float y, float z)
        {
            Position = new Vector3(x, y, z);
            Color = Color.Black;
            Normal = new Vector3();
        }

        public VertexPositionColorNormal(float x, float y, float z, Color color)
        {
            Position = new Vector3(x, y, z);
            Color = color;
            Normal = new Vector3();
        }
    }
}
