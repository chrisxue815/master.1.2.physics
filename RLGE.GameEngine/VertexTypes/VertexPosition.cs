﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLGE.GameEngine.VertexTypes
{
    public struct VertexPosition : IVertexType
    {
        public Vector3 Position;

        public static readonly VertexDeclaration Declaration = new VertexDeclaration
        (
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0)
        );

        public VertexDeclaration VertexDeclaration { get { return Declaration; } }

        public VertexPosition(Vector3 position)
        {
            Position = position;
        }

        public VertexPosition(float x, float y, float z)
        {
            Position = new Vector3(x, y, z);
        }
    }
}
