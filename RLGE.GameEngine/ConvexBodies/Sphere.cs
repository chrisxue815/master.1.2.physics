﻿using Microsoft.Xna.Framework;
using RLGE.GameEngine.Renderers;
using RLGE.Physics.RigidBodyEngine.Colliders;

namespace RLGE.GameEngine.ConvexBodies
{
    public class Sphere : ConvexBody
    {
        public Sphere(GameBase game, Vector3 scale)
            : base(game)
        {
            Collider = new SphereCollider(RigidBody, scale);
        }

        public Sphere(GameBase game, float width, float height, float depth)
            : base(game)
        {
            Collider = new SphereCollider(RigidBody, width, height, depth);
        }

        public Sphere(GameBase game, float scale)
            : base(game)
        {
            Collider = new SphereCollider(RigidBody, scale);
        }

        public Sphere(GameBase game, Vector3 scale, float mass)
            : base(game)
        {
            Collider = new SphereCollider(RigidBody, scale, mass);
        }

        public Sphere(GameBase game, float width, float height, float depth, float mass)
            : base(game)
        {
            Collider = new SphereCollider(RigidBody, width, height, depth, mass);
        }

        public Sphere(GameBase game, float scale, float mass)
            : base(game)
        {
            Collider = new SphereCollider(RigidBody, scale, mass);
        }

        public override void Initialize()
        {
            renderer = new SphereModelRenderer(Game);
            renderer.Initialize();

            base.Initialize();
        }
    }
}
