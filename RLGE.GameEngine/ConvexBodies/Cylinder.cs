﻿using Microsoft.Xna.Framework;
using RLGE.GameEngine.Renderers;
using RLGE.Physics.RigidBodyEngine.Colliders;

namespace RLGE.GameEngine.ConvexBodies
{
    public class Cylinder : ConvexBody
    {
        public Cylinder(GameBase game, Vector3 scale)
            : base(game)
        {
            Collider = new CylinderCollider(RigidBody, scale);
        }

        public Cylinder(GameBase game, float width, float height, float depth)
            : base(game)
        {
            Collider = new CylinderCollider(RigidBody, width, height, depth);
        }

        public Cylinder(GameBase game, float scale)
            : base(game)
        {
            Collider = new CylinderCollider(RigidBody, scale);
        }

        public Cylinder(GameBase game, Vector3 scale, float mass)
            : base(game)
        {
            Collider = new CylinderCollider(RigidBody, scale, mass);
        }

        public Cylinder(GameBase game, float width, float height, float depth, float mass)
            : base(game)
        {
            Collider = new CylinderCollider(RigidBody, width, height, depth, mass);
        }

        public Cylinder(GameBase game, float scale, float mass)
            : base(game)
        {
            Collider = new CylinderCollider(RigidBody, scale, mass);
        }

        public override void Initialize()
        {
            renderer = new CylinderModelRenderer(Game);
            renderer.Initialize();

            base.Initialize();
        }
    }
}
