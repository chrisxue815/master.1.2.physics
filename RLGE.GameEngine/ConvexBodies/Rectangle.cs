﻿using RLGE.GameEngine.Renderers;
using RLGE.Physics.RigidBodyEngine.Colliders;

namespace RLGE.GameEngine.ConvexBodies
{
    public class Rectangle : ConvexBody
    {
        public Rectangle(GameBase game, float width, float depth, float mass)
            : base(game)
        {
            Collider = new RectangleCollider(RigidBody, width, depth, mass);
        }

        public Rectangle(GameBase game, float width, float depth)
            : base(game)
        {
            Collider = new RectangleCollider(RigidBody, width, depth);
        }

        public override void Initialize()
        {
            renderer = new RectangleRenderer(Game);
            renderer.Initialize();

            isBound = true;

            base.Initialize();
        }
    }
}
