﻿using Microsoft.Xna.Framework;
using RLGE.GameEngine.Renderers;
using RLGE.Physics.RigidBodyEngine.Colliders;

namespace RLGE.GameEngine.ConvexBodies
{
    public class Cube : ConvexBody
    {
        public Cube(GameBase game, Vector3 scale)
            : base(game)
        {
            Collider = new CubeCollider(RigidBody, scale);
        }

        public Cube(GameBase game, float width, float height, float depth)
            : base(game)
        {
            Collider = new CubeCollider(RigidBody, width, height, depth);
        }

        public Cube(GameBase game, float scale)
            : base(game)
        {
            Collider = new CubeCollider(RigidBody, scale);
        }

        public Cube(GameBase game, Vector3 scale, float mass)
            : base(game)
        {
            Collider = new CubeCollider(RigidBody, scale, mass);
        }

        public Cube(GameBase game, float width, float height, float depth, float mass)
            : base(game)
        {
            Collider = new CubeCollider(RigidBody, width, height, depth, mass);
        }

        public Cube(GameBase game, float scale, float mass)
            : base(game)
        {
            Collider = new CubeCollider(RigidBody, scale, mass);
        }

        public override void Initialize()
        {
            renderer = new CubeModelRenderer(Game);
            renderer.Initialize();

            base.Initialize();
        }
    }
}
