﻿using Microsoft.Xna.Framework;
using RLGE.GameEngine.Renderers;
using RLGE.Physics;
using RLGE.Physics.RigidBodyEngine.Colliders;

namespace RLGE.GameEngine.ConvexBodies
{
    public class ConvexBody : GameEntity
    {
        public Collider Collider;
        public RigidBody RigidBody;

        public override Transform Transform { get { return RigidBody; } }

        protected Renderer renderer;
        protected CubeSkeletonRenderer boundingBoxRenderer;

        protected bool isBound;

        public ConvexBody(GameBase game)
            : base(game)
        {
            RigidBody = new RigidBody();
        }

        public override void Initialize()
        {
            boundingBoxRenderer = new CubeSkeletonRenderer(Game);
            boundingBoxRenderer.Initialize();
        }

        public override void Draw()
        {
            var alpha = Game.Controller.ShowingContact || Game.Controller.ShowingBoundsContact ? 0.5f : 1;
            renderer.Draw(RigidBody, alpha);

            var showingBoundingBox = isBound
                ? Game.Controller.ShowingBoundsBounding
                : Game.Controller.ShowingBounding;

            if (showingBoundingBox)
            {
                var world = Matrix.CreateScale(Collider.AABB.Scale);
                world *= Matrix.CreateTranslation(RigidBody.Position);

                var color = RigidBody.BroadPhaseCollisionDetected ? Color.Red : Color.Black;
                boundingBoxRenderer.Draw(world, color.ToVector3());
            }
        }
    }
}
