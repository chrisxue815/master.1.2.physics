﻿namespace RLGE.GameEngine
{
    public abstract class GameObject
    {
        protected GameBase Game { get; set; }

        protected GameObject(GameBase game)
        {
            Game = game;
        }

        public virtual void Initialize()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void Draw()
        {
        }
    }
}
