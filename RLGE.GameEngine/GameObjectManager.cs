﻿using System.Collections;
using System.Collections.Generic;

namespace RLGE.GameEngine
{
    public class GameObjectManager : GameObjectManager<GameObject>
    {
        public GameObjectManager(GameBase game)
            : base(game)
        {
        }
    }

    public class GameObjectManager<T> : GameObject, IEnumerable<T>
        where T : GameObject
    {
        public List<T> GameObjects { get; protected set; }

        public GameObjectManager(GameBase game)
            : base(game)
        {
            GameObjects = new List<T>();
        }

        public override void Initialize()
        {
            foreach (var gameComponent in GameObjects)
            {
                gameComponent.Initialize();
            }
        }

        public override void Update()
        {
            foreach (var gameComponent in GameObjects)
            {
                gameComponent.Update();
            }
        }

        public override void Draw()
        {
            foreach (var gameComponent in GameObjects)
            {
                gameComponent.Draw();
            }
        }

        public void Add(T obj)
        {
            GameObjects.Add(obj);
        }

        public void Remove(T obj)
        {
            GameObjects.Remove(obj);
        }

        public void Clear()
        {
            GameObjects.Clear();
        }

        public int Count
        {
            get { return GameObjects.Count; }
        }

        public T this[int i]
        {
            get { return GameObjects[i]; }
            set { GameObjects[i] = value; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return GameObjects.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
