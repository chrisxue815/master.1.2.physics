﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLGE.GameEngine.Entities
{
    public class Help : GameObject
    {
        private SpriteFont spriteFont;
        private readonly List<string> texts;

        public Help(GameBase game)
            : base(game)
        {
            texts = new List<string>
            {
                "Left button: Rotate camera",
                "Scroll wheel: Zoom in/out",
                "1: Add a random convex body",
                "2: Add a cube",
                "3: Add a sphere",
                "4: Add a cylinder",
                "C: Clear the scene",
                "Q: Show/hide bounding box of bodies",
                "W: Show/hide contact model of bodies",
                "A: Show/hide bounding box of bounds",
                "S: Show/hide contact model of bounds",
                "Space: Pause/continue"
            };
        }

        public override void Initialize()
        {
            spriteFont = Game.Content.Load<SpriteFont>("fonts/monitor");
        }

        public override void Draw()
        {
            const string help = "H: Toggle help";
            var pos = new Vector2(10, Game.Monitor.MaxY + 20);

            Game.SpriteBatch.DrawString(spriteFont, help, pos, Color.Black);

            pos += new Vector2(0, 30);

            if (Game.Controller.ShowingHelp)
            {
                foreach (var text in texts)
                {
                    Game.SpriteBatch.DrawString(spriteFont, text, pos, Color.Black);

                    pos += new Vector2(0, 20);
                }
            }
        }

        public void Add(string text)
        {
            texts.Add(text);
        }
    }
}
