﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.ConvexBodies;
using RLGE.GameEngine.VertexTypes;
using RLGE.Physics.RigidBodyEngine.Colliders;

namespace RLGE.GameEngine.Entities
{
    public class GridLineGround : ConvexBody
    {
        private readonly int numGridX;
        private readonly int numGridZ;
        private readonly float gridWidth;

        private VertexBuffer vertexBuffer;
        private BasicEffect effect;
        private int numLines;

        public GridLineGround(GameBase game, int numGridX, int numGridZ, float gridWidth)
            : base(game)
        {
            this.numGridX = numGridX;
            this.numGridZ = numGridZ;
            this.gridWidth = gridWidth;
            var width = numGridX * gridWidth;
            var depth = numGridZ * gridWidth;
            Collider = new RectangleCollider(RigidBody, width, depth);
        }

        public override void Initialize()
        {
            CreateBuffers();

            effect = new BasicEffect(Game.GraphicsDevice);
        }

        private void CreateBuffers()
        {
            var right = numGridX * gridWidth / 2;
            var left = -right;
            var bottom = numGridZ * gridWidth / 2;
            var top = -bottom;

            numLines = numGridX + numGridZ + 2;
            var numVertices = numLines * 2;
            var vertices = new VertexPosition[numVertices];
            var iVertice = 0;

            for (var x = left; x <= right; x += gridWidth)
            {
                vertices[iVertice++] = new VertexPosition(x, 0, top);
                vertices[iVertice++] = new VertexPosition(x, 0, bottom);
            }

            for (var y = top; y <= bottom; y += gridWidth)
            {
                vertices[iVertice++] = new VertexPosition(left, 0, y);
                vertices[iVertice++] = new VertexPosition(right, 0, y);
            }

            vertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPosition), vertices.Length, BufferUsage.None);
            vertexBuffer.SetData(vertices);
        }

        public override void Draw()
        {
            effect.World = Matrix.CreateTranslation(Transform.Position);
            effect.View = Game.Camera.View;
            effect.Projection = Game.Camera.Projection;
            effect.DiffuseColor = RigidBody.DiffuseColor;

            Game.GraphicsDevice.SetVertexBuffer(vertexBuffer);

            Game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            foreach (var pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                Game.GraphicsDevice.DrawPrimitives(PrimitiveType.LineList, 0, numLines);
            }
        }
    }
}
