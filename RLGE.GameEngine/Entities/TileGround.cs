﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.VertexTypes;
using RLGE.Physics;

namespace RLGE.GameEngine.Entities
{
    public class TileGround : GameEntity
    {
        private readonly int numGridX;
        private readonly int numGridZ;
        private readonly float gridWidth;

        private VertexBuffer VertexBuffer { get; set; }
        private IndexBuffer IndexBuffer { get; set; }
        private BasicEffect BasicEffect { get; set; }
        private int NumTriangles { get; set; }

        public TileGround(GameBase game, int numGridX, int numGridZ, float gridWidth)
            : base(game)
        {
            this.numGridX = numGridX;
            this.numGridZ = numGridZ;
            this.gridWidth = gridWidth;
        }

        public override void Initialize()
        {
            CreateBuffers();

            BasicEffect = new BasicEffect(Game.GraphicsDevice);
        }

        private void CreateBuffers()
        {
            var right = numGridX * gridWidth / 2;
            var left = -right;
            var bottom = numGridZ * gridWidth / 2;
            var top = -bottom;
            var numVertices = (numGridX + 1) * (numGridZ + 1);
            var vertices = new VertexPosition[numVertices];
            var iVertice = 0;

            for (var y = top; y <= bottom; y += gridWidth)
            {
                for (var x = left; x <= right; x += gridWidth)
                {
                    vertices[iVertice++] = new VertexPosition(x, 0, y);
                }
            }

            VertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPosition), vertices.Length, BufferUsage.None);
            VertexBuffer.SetData(vertices);

            var numSquares = numGridX * numGridZ / 2 + numGridX * numGridZ % 2;
            NumTriangles = numSquares * 2;
            var numIndices = NumTriangles * 3;
            var indices = new short[numIndices];
            var iIndex = 0;

            for (var j = 0; j < numGridZ; j++)
            {
                for (var i = 0; i < numGridX; i++)
                {
                    if ((i + j) % 2 != 0)
                    {
                        continue;
                    }

                    var topLeft = (short)(j * (numGridX + 1) + i);
                    var topRight = (short)(topLeft + 1);
                    var bottomLeft = (short)((j + 1) * (numGridX + 1) + i);
                    var bottomRight = (short)(bottomLeft + 1);

                    indices[iIndex++] = topLeft;
                    indices[iIndex++] = topRight;
                    indices[iIndex++] = bottomRight;
                    indices[iIndex++] = bottomRight;
                    indices[iIndex++] = bottomLeft;
                    indices[iIndex++] = topLeft;
                }
            }

            IndexBuffer = new IndexBuffer(Game.GraphicsDevice, IndexElementSize.SixteenBits, indices.Length, BufferUsage.None);
            IndexBuffer.SetData(indices);
        }

        public override void Draw()
        {
            BasicEffect.World = Matrix.CreateTranslation(Transform.Position);
            BasicEffect.View = Game.Camera.View;
            BasicEffect.Projection = Game.Camera.Projection;
            BasicEffect.DiffuseColor = Color.DarkBlue.ToVector3();

            Game.GraphicsDevice.Indices = IndexBuffer;
            Game.GraphicsDevice.SetVertexBuffer(VertexBuffer);

            Game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            foreach (var pass in BasicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                Game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, VertexBuffer.VertexCount, 0,
                    NumTriangles);
            }
        }
    }
}
