﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLGE.GameEngine.Entities
{
    public class Monitor : GameObject
    {
        private SpriteFont spriteFont;

        private readonly List<string> texts;
        public float MaxY;

        public Monitor(GameBase game)
            : base(game)
        {
            texts = new List<string>();
        }

        public override void Initialize()
        {
            spriteFont = Game.Content.Load<SpriteFont>("fonts/monitor");
        }

        public override void Update()
        {
            texts.Clear();

            var fps = string.Format("FPS: {0:f1}", Game.FrameRate);
            Add(fps);
        }

        public override void Draw()
        {
            var pos = new Vector2(10, 10);

            foreach (var text in texts)
            {
                Game.SpriteBatch.DrawString(spriteFont, text, pos, Color.Black);

                pos += new Vector2(0, 20);
            }

            if (pos.Y > MaxY) MaxY = pos.Y;
        }

        public void Add(string text)
        {
            texts.Add(text);
        }
    }
}
