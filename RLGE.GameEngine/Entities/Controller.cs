﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Input;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace RLGE.GameEngine.Entities
{
    public class Controller : GameObject, IEnumerable<Keys>
    {
        public bool ShowingHelp;
        public bool ShowingBounding;
        public bool ShowingContact;
        public bool ShowingBoundsBounding;
        public bool ShowingBoundsContact;

        private readonly Dictionary<Keys, bool> keyMap;
        private Keys[] PreviousKeys { get; set; }

        public Controller(GameBase game)
            : base(game)
        {
            keyMap = new Dictionary<Keys, bool>();
        }

        public override void Update()
        {
            var keyState = Keyboard.GetState();
            var keys = keyState.GetPressedKeys();
            keyMap.Clear();

            foreach (var key in keys)
            {
                if (PreviousKeys.Contains(key)) continue;

                keyMap[key] = true;

                switch (key)
                {
                    case Keys.Q:
                        ShowingBounding = !ShowingBounding;
                        break;
                    case Keys.W:
                        ShowingContact = !ShowingContact;
                        break;
                    case Keys.A:
                        ShowingBoundsBounding = !ShowingBoundsBounding;
                        break;
                    case Keys.S:
                        ShowingBoundsContact = !ShowingBoundsContact;
                        break;
                    case Keys.H:
                        ShowingHelp = !ShowingHelp;
                        break;
                }
            }

            PreviousKeys = keys;
        }

        public bool this[Keys key]
        {
            get { return keyMap.ContainsKey(key); }
            set
            {
                if (value) keyMap[key] = true;
                else keyMap.Remove(key);
            }
        }

        public IEnumerator<Keys> GetEnumerator()
        {
            foreach (var key in keyMap.Keys)
            {
                yield return key;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
