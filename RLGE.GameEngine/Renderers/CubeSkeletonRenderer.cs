﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.VertexTypes;

namespace RLGE.GameEngine.Renderers
{
    public class CubeSkeletonRenderer : Renderer
    {
        private VertexPositionNormal[] Vertices { get; set; }
        private short[] Indices { get; set; }

        private VertexBuffer VertexBuffer { get; set; }
        private IndexBuffer IndexBuffer { get; set; }

        private BasicEffect BasicEffect { get; set; }

        public CubeSkeletonRenderer(GameBase game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            CreateBuffers();

            BasicEffect = new BasicEffect(Game.GraphicsDevice);
        }

        private void CreateBuffers()
        {
            Vertices = new[]
            {
                new VertexPositionNormal(-0.5f, -0.5f, -0.5f),
                new VertexPositionNormal(+0.5f, -0.5f, -0.5f),
                new VertexPositionNormal(+0.5f, +0.5f, -0.5f),
                new VertexPositionNormal(-0.5f, +0.5f, -0.5f),
                new VertexPositionNormal(-0.5f, -0.5f, +0.5f),
                new VertexPositionNormal(+0.5f, -0.5f, +0.5f),
                new VertexPositionNormal(+0.5f, +0.5f, +0.5f),
                new VertexPositionNormal(-0.5f, +0.5f, +0.5f)
            };

            Indices = new short[]
            {
                0,1,
                1,2,
                2,3,
                0,3,
                0,4,
                1,5,
                2,6,
                3,7,
                4,5,
                5,6,
                6,7,
                4,7
            };

            for (var i = 0; i < Indices.Length; i += 2)
            {
                int index1 = Indices[i];
                int index2 = Indices[i + 1];

                var normal = Vertices[index1].Position - Vertices[index2].Position;
                normal.Normalize();

                Vertices[index1].Normal += normal;
                Vertices[index2].Normal += normal;
            }

            for (var i = 0; i < Vertices.Length; i++)
            {
                Vertices[i].Normal.Normalize();
            }

            VertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPositionNormal), Vertices.Length, BufferUsage.None);
            VertexBuffer.SetData(Vertices);

            IndexBuffer = new IndexBuffer(Game.GraphicsDevice, IndexElementSize.SixteenBits, Indices.Length, BufferUsage.None);
            IndexBuffer.SetData(Indices);
        }

        public override void Draw(Matrix world, Vector3 diffuseColor, float alpha)
        {
            BasicEffect.World = world;
            BasicEffect.View = Game.Camera.View;
            BasicEffect.Projection = Game.Camera.Projection;
            BasicEffect.DiffuseColor = diffuseColor;
            BasicEffect.Alpha = alpha;
            //BasicEffect.EnableDefaultLighting();

            Game.GraphicsDevice.Indices = IndexBuffer;
            Game.GraphicsDevice.SetVertexBuffer(VertexBuffer);

            foreach (var pass in BasicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                Game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, VertexBuffer.VertexCount, 0, 12);
            }
        }
    }
}
