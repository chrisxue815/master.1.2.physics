﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLGE.GameEngine.Renderers
{
    public class SphereModelRenderer : Renderer
    {
        private Model Model { get; set; }

        public SphereModelRenderer(GameBase game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            Model = Game.Content.Load<Model>("models/sphere");
        }

        public override void Draw(Matrix world, Vector3 diffuseColor, float alpha)
        {
            foreach (var mesh in Model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = world;
                    effect.View = Game.Camera.View;
                    effect.Projection = Game.Camera.Projection;
                    effect.DiffuseColor = diffuseColor;
                    effect.EnableDefaultLighting();
                    effect.SpecularPower = 100;
                    effect.Alpha = alpha;
                }
                mesh.Draw();
            }
        }
    }
}
