﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.Physics;

namespace RLGE.GameEngine.Renderers
{
    public class ParticleEffectRenderer : GameObject
    {
        private Model Model { get; set; }
        private Texture2D Texture { get; set; }

        public ParticleEffectRenderer(GameBase game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            Model = Game.Content.Load<Model>("models/sphere");
            Texture = Game.Content.Load<Texture2D>("textures/Water001");
        }

        public void Draw(RigidBody particle)
        {
            /*
            VertexPositionTexture[]
            verts ={new VertexPositionTexture(new Vector3(Position.X,   y,   0), new Vector2(0.0f, 0.0f)),
            new VertexPositionTexture(new Vector3(x+w, y,   0), new Vector2(1.0f, 0.0f)),
            new VertexPositionTexture(new Vector3(x+w, y+h, 0), new Vector2(1.0f, 1.0f)),
            new VertexPositionTexture(new Vector3(x,   y+h, 0), new Vector2(0.0f, 1.0f))};

            var effect = new BasicEffect(Game.GraphicsDevice);
            effect.Alpha = 1.0f;
            effect.TextureEnabled = true;
            effect.Texture = Texture;
            effect.EnableDefaultLighting();
            effect.LightingEnabled = true;
            effect.World = Matrix.CreateScale(0.05f) * Matrix.CreateTranslation(particle.Position);
            effect.View = Game.Camera.View;
            effect.Projection = Game.Camera.Projection;
            Game.GraphicsDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
            Game.GraphicsDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;
            Game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            foreach (EffectPass pass in ParticleEffect.CurrentTechnique.Passes)
            {
                Game.GraphicsDevice.DrawUserPrimitives<VertexPositionTexture>(
                            PrimitiveType.TriangleFan,
                            verts, 0, 2);

                pass.Apply();
            }

            continue;*/
            foreach (var mesh in Model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = Matrix.CreateScale(0.05f) * Matrix.CreateTranslation(particle.Position);
                    effect.View = Game.Camera.View;
                    effect.Projection = Game.Camera.Projection;
                    effect.DiffuseColor = particle.DiffuseColor;
                    effect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }
    }
}
