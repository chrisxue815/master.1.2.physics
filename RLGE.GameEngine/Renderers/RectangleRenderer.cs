﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.VertexTypes;

namespace RLGE.GameEngine.Renderers
{
    public class RectangleRenderer : Renderer
    {
        private VertexBuffer vertexBuffer;
        private BasicEffect effect;

        public RectangleRenderer(GameBase game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            CreateBuffers();

            effect = new BasicEffect(Game.GraphicsDevice);
        }

        private void CreateBuffers()
        {
            var vertices = new[]
            {
                new VertexPositionNormal(0.5f, 0, 0.5f, Vector3.Up),
                new VertexPositionNormal(0.5f, 0, -0.5f, Vector3.Up),
                new VertexPositionNormal(-0.5f, 0, -0.5f, Vector3.Up),
                new VertexPositionNormal(-0.5f, 0, 0.5f, Vector3.Up),
                new VertexPositionNormal(0.5f, 0, 0.5f, Vector3.Up)
            };

            vertexBuffer = new VertexBuffer(Game.GraphicsDevice, vertices[0].GetType(), vertices.Length, BufferUsage.None);
            vertexBuffer.SetData(vertices);
        }

        public override void Draw(Matrix world, Vector3 diffuseColor, float alpha)
        {
            effect.World = world;
            effect.View = Game.Camera.View;
            effect.Projection = Game.Camera.Projection;
            effect.DiffuseColor = diffuseColor;
            //effect.EnableDefaultLighting();

            Game.GraphicsDevice.SetVertexBuffer(vertexBuffer);

            Game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            foreach (var pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                Game.GraphicsDevice.DrawPrimitives(PrimitiveType.LineStrip, 0, vertexBuffer.VertexCount - 1);
            }
        }
    }
}
