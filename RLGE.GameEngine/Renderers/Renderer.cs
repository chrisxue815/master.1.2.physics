﻿using Microsoft.Xna.Framework;
using RLGE.Physics;

namespace RLGE.GameEngine.Renderers
{
    public abstract class Renderer : GameObject
    {
        protected Renderer(GameBase game) : base(game)
        {
        }

        public abstract void Draw(Matrix world, Vector3 diffuseColor, float alpha);

        public virtual void Draw(RigidBody rigidBody)
        {
            Draw(rigidBody.WorldTransform, rigidBody.DiffuseColor, rigidBody.Alpha);
        }

        public virtual void Draw(RigidBody rigidBody, float alpha)
        {
            Draw(rigidBody.WorldTransform, rigidBody.DiffuseColor, alpha);
        }

        public virtual void Draw(Matrix world, Vector3 diffuseColor)
        {
            Draw(world, diffuseColor, 1);
        }
    }
}
