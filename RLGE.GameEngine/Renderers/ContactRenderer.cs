﻿using RLGE.Physics.RigidBodyEngine.Colliders;
using RLGE.Physics.RigidBodyEngine.Constraints;

namespace RLGE.GameEngine.Renderers
{
    public class ContactRenderer : GameObject
    {
        private readonly SolverManager solverManager;
        private readonly Arrow arrow;

        public ContactRenderer(GameBase game, SolverManager solverManager)
            : base(game)
        {
            this.solverManager = solverManager;
            arrow = new Arrow(game);
        }

        public override void Initialize()
        {
            arrow.Initialize();
        }

        public override void Draw()
        {
            foreach (var solver in solverManager)
            {
                foreach (var contact in solver)
                {
                    var isBound = contact.ColliderA is RectangleCollider || contact.ColliderB is RectangleCollider;
                    var showingContactModel = isBound ? Game.Controller.ShowingBoundsContact : Game.Controller.ShowingContact;

                    if (showingContactModel)
                    {
                        arrow.Draw(contact.Point, contact.Normal);
                    }
                }
            }
        }
    }
}
