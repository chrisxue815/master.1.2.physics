﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLGE.GameEngine.Renderers
{
    public class Arrow : GameObject
    {
        private Model SphereModel { get; set; }
        private Model CubeModel { get; set; }

        public Arrow(GameBase game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            SphereModel = Game.Content.Load<Model>("models/sphere");
            CubeModel = Game.Content.Load<Model>("models/cube");
        }

        public void Draw(Vector3 position, Vector3 direction)
        {
            var angle = (float)Math.Acos(Vector3.Dot(direction, Vector3.Forward) / direction.Length());
            var axis = Vector3.Cross(Vector3.Forward, direction);
            axis.Normalize();
            var orientation = Quaternion.CreateFromAxisAngle(axis, angle);

            foreach (var mesh in SphereModel.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = Matrix.CreateScale(0.1f) * Matrix.CreateTranslation(position);
                    effect.View = Game.Camera.View;
                    effect.Projection = Game.Camera.Projection;
                    effect.DiffuseColor = Color.Red.ToVector3();
                    effect.Alpha = 0.5f;
                }
                mesh.Draw();
            }

            foreach (var mesh in CubeModel.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = Matrix.CreateTranslation(new Vector3(0, 0, -0.5f)) *
                        Matrix.CreateScale(0.02f, 0.02f, 1f) *
                        Matrix.CreateFromQuaternion(orientation) *
                        Matrix.CreateTranslation(position);
                    effect.View = Game.Camera.View;
                    effect.Projection = Game.Camera.Projection;
                    effect.DiffuseColor = Color.Red.ToVector3();
                    effect.Alpha = 0.5f;
                }
                mesh.Draw();
            }
        }
    }
}
