﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.VertexTypes;

namespace RLGE.GameEngine.Renderers
{
    public class CubeRenderer : Renderer
    {
        private Vector3[] RawVertices { get; set; }
        private VertexPositionNormal[] Vertices { get; set; }
        private short[] Indices { get; set; }

        private VertexBuffer VertexBuffer { get; set; }
        private IndexBuffer IndexBuffer { get; set; }

        private BasicEffect BasicEffect { get; set; }

        public CubeRenderer(GameBase game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            CreateBuffers();

            BasicEffect = new BasicEffect(Game.GraphicsDevice);
        }

        private void CreateBuffers()
        {
            RawVertices = new[]
            {
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(+0.5f, -0.5f, -0.5f),
                new Vector3(+0.5f, +0.5f, -0.5f),
                new Vector3(-0.5f, +0.5f, -0.5f),
                new Vector3(-0.5f, -0.5f, +0.5f),
                new Vector3(+0.5f, -0.5f, +0.5f),
                new Vector3(+0.5f, +0.5f, +0.5f),
                new Vector3(-0.5f, +0.5f, +0.5f)
            };

            Vertices = new VertexPositionNormal[24];

            for (var i = 0; i < 8; i++)
            {
                Vertices[i] = new VertexPositionNormal(RawVertices[i]);
                Vertices[8 + i] = new VertexPositionNormal(RawVertices[i]);
                Vertices[16 + i] = new VertexPositionNormal(RawVertices[i]);
            }

            Indices = new short[]
            {
                0,1,2,
                2,3,0,
                5,4,7,
                7,6,5,
                0,3,7,
                7,4,0,
                1,5,6,
                6,2,1,
                0,4,5,
                5,1,0,
                2,6,7,
                7,3,2
            };

            for (var i = 0; i < 12; i++)
            {
                Indices[i] += 8;
                Indices[i + 12] += 16;
            }

            for (var i = 0; i < Indices.Length; i += 3)
            {
                int index1 = Indices[i];
                int index2 = Indices[i + 1];
                int index3 = Indices[i + 2];

                var side1 = Vertices[index2].Position - Vertices[index1].Position;
                var side2 = Vertices[index2].Position - Vertices[index3].Position;
                var normal = Vector3.Cross(side1, side2);
                normal.Normalize();

                Vertices[index1].Normal += normal;
                Vertices[index2].Normal += normal;
                Vertices[index3].Normal += normal;
            }

            for (var i = 0; i < Vertices.Length; i++)
            {
                Vertices[i].Normal.Normalize();
            }

            VertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPositionNormal), Vertices.Length, BufferUsage.None);
            VertexBuffer.SetData(Vertices);

            IndexBuffer = new IndexBuffer(Game.GraphicsDevice, IndexElementSize.SixteenBits, Indices.Length, BufferUsage.None);
            IndexBuffer.SetData(Indices);
        }

        public override void Draw(Matrix world, Vector3 diffuseColor, float alpha)
        {
            BasicEffect.World = world;
            BasicEffect.View = Game.Camera.View;
            BasicEffect.Projection = Game.Camera.Projection;
            BasicEffect.DiffuseColor = diffuseColor;
            BasicEffect.SpecularColor = diffuseColor;
            BasicEffect.EnableDefaultLighting();
            BasicEffect.Alpha = alpha;

            Game.GraphicsDevice.Indices = IndexBuffer;
            Game.GraphicsDevice.SetVertexBuffer(VertexBuffer);

            foreach (var pass in BasicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                Game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, VertexBuffer.VertexCount, 0, 12);
            }
        }
    }
}
