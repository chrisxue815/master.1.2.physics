using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RLGE.GameEngine.Entities;

namespace RLGE.GameEngine
{
    public class GameBase : Game
    {
        public Controller Controller { get; protected set; }
        public Camera Camera { get; protected set; }
        public Monitor Monitor { get; protected set; }

        protected List<GameObject> GameObjects { get; set; }

        public long ElapsedTicks { get; protected set; }
        public long TotalTicks { get; protected set; }
        public float ElapsedSeconds { get; protected set; }
        public float TotalSeconds { get; protected set; }

        public GraphicsDeviceManager Graphics;
        public SpriteBatch SpriteBatch;

        public bool Paused;
        public bool WasPaused;

        public float FrameRate;
        private int frameCount;
        private float timeCount;
        private DateTime previousTime;

        public GameBase()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            GameObjects = new List<GameObject>();

            Controller = new Controller(this);
            GameObjects.Add(Controller);

            Camera = new Camera(this);
            Camera.LookAt(new Vector3(0, 0, -5), Vector3.Zero);
            GameObjects.Add(Camera);

            Monitor = new Monitor(this);
            GameObjects.Add(Monitor);

            Paused = false;
            WasPaused = false;
        }

        protected override void Initialize()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            foreach (var gameObject in GameObjects)
            {
                gameObject.Initialize();
            }

            previousTime = DateTime.Now;

            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            ElapsedTicks = gameTime.ElapsedGameTime.Ticks;
            TotalTicks = gameTime.TotalGameTime.Ticks;

            ElapsedSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;
            TotalSeconds = (float)gameTime.TotalGameTime.TotalSeconds;

            Update();

            foreach (var gameObject in GameObjects)
            {
                gameObject.Update();
            }

            base.Update(gameTime);
        }

        protected virtual void Update()
        {
        }

        protected override void Draw(GameTime gameTime)
        {
            UpdateFPS();

            GraphicsDevice.Clear(Color.Gray);

            SpriteBatch.Begin();

            GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            Draw();

            foreach (var gameObject in GameObjects)
            {
                gameObject.Draw();
            }

            SpriteBatch.End();

            base.Draw(gameTime);
        }

        protected virtual void Draw()
        {
        }

        protected void SetBorderlessWindow()
        {
            var hWnd = Window.Handle;
            var control = System.Windows.Forms.Control.FromHandle(hWnd);
            var form = control.FindForm();
            if (form == null) return;
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            form.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }

        private void UpdateFPS()
        {
            var now = DateTime.Now;
            frameCount++;
            timeCount += (float)(now - previousTime).TotalSeconds;

            if (timeCount >= 1)
            {
                FrameRate = frameCount / timeCount;
                frameCount = 0;
                timeCount = 0;
            }

            previousTime = now;
        }
    }
}
