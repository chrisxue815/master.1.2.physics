﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.Colliders;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine.Collisions.MPR
{
    public static class MPRToolbox
    {
        public const int OuterIterationLimit = 15;
        public const float MinCollisionDepth = 0.1f;
        public const float CollideEpsilon = 1e-3f;

        public static bool FindCollision(Collider a, Collider b, ref Vector3 pointA, ref Vector3 pointB, ref Vector3 normal)
        {
            // v0 = center of Minkowski sum
            var v01 = a.RigidBody.Position;
            var v02 = b.RigidBody.Position;
            var v0 = v02 - v01;

            // Avoid case where centers overlap -- any direction is fine in this case
            if (v0.IsZero()) v0 = new Vector3(0.00001f, 0, 0);

            // v1 = support in direction of origin
            var n1 = -v0;
            Vector3 v11, v12;
            a.FindSupport(ref v0, out v11);
            b.FindSupport(ref n1, out v12);
            var v1 = v12 - v11;

            if (Vector3.Dot(v1, n1) <= 0)
            {
                normal = n1;
                return false;
            }

            // v2 - support perpendicular to v1,v0
            var n2 = Vector3.Cross(v1, v0);
            if (n2.IsZero())
            {
                n2 = v1 - v0;
                n2.Normalize();
                normal = n2;
                pointA = v11;
                pointB = v12;
                return true;
            }

            Vector3 v21, v22;
            var tmp = -n2;
            a.FindSupport(ref tmp, out v21);
            b.FindSupport(ref n2, out v22);
            var v2 = v22 - v21;

            if (Vector3.Dot(v2, n2) <= 0)
            {
                normal = n2;
                return false;
            }

            // Determine whether origin is on + or - side of plane (v1,v0,v2)
            var n3 = Vector3.Cross(v1 - v0, v2 - v0);
            var dist = Vector3.Dot(n3, v0);

            if (n3.IsZero())
            {
                tmp = -n1;
                a.FindSupport(ref tmp, out v11);
                b.FindSupport(ref n1, out v12);
                v1 = v12 - v11;
                tmp = -n2;
                a.FindSupport(ref tmp, out v21);
                b.FindSupport(ref n2, out v22);
                v2 = v22 - v21;
            }
            Debug.Assert(!n3.IsZero());

            // If the origin is on the - side of the plane, reverse the direction of the plane
            if (dist > 0)
            {
                Utils.Swap(ref v1, ref v2);
                Utils.Swap(ref v11, ref v21);
                Utils.Swap(ref v12, ref v22);
                n3 = -n3;
            }

            //
            // Phase One: Identify a portal

            for (; ; )
            {
                // Obtain the support point in a direction perpendicular to the existing plane
                // Note: This point is guaranteed to lie off the plane
                Vector3 v31, v32;
                tmp = -n3;
                a.FindSupport(ref tmp, out v31);
                b.FindSupport(ref n3, out v32);
                var v3 = v32 - v31;

                if (Vector3.Dot(v3, n3) <= 0)
                {
                    normal = n3;
                    return false;
                }

                // If origin is outside (v1,v0,v3), then eliminate v2 and loop
                if (Vector3.Dot(Vector3.Cross(v1, v3), v0) < 0)
                {
                    v2 = v3;
                    v21 = v31;
                    v22 = v32;
                    n3 = Vector3.Cross(v1 - v0, v3 - v0);
                    continue;
                }

                // If origin is outside (v3,v0,v2), then eliminate v1 and loop
                if (Vector3.Dot(Vector3.Cross(v3, v2), v0) < 0)
                {
                    v1 = v3;
                    v11 = v31;
                    v12 = v32;
                    n3 = Vector3.Cross(v3 - v0, v2 - v0);
                    continue;
                }

                var hit = false;

                //
                // Phase Two: Refine the portal

                var phase2 = 0;

                // We are now inside of a wedge...
                for (; ; )
                {
                    phase2++;
                    if (phase2 > 1)
                    {
                        //TODO
                        doneIt = false;
                        var trackingOn = true;
                        if (!trackingOn && !doneIt)
                        {
                            doneIt = true;
                            return false;
                        }
                    }

                    // Compute normal of the wedge face
                    var n4 = Vector3.Cross(v2 - v1, v3 - v1);

                    // Can this happen???  Can it be handled more cleanly?
                    if (n4.IsZero())
                    {
                        //TODO
                        return false;
                        Debug.Assert(false);
                        return true;
                    }

                    n4.Normalize();

                    // Compute distance from origin to wedge face
                    var d = Vector3.Dot(n4, v1);

                    // If the origin is inside the wedge, we have a hit
                    if (d >= 0 && !hit)
                    {
                        normal = n4;

                        // Compute the barycentric coordinates of the origin
                        var b0 = Vector3.Dot(Vector3.Cross(v1, v2), v3);
                        var b1 = Vector3.Dot(Vector3.Cross(v3, v2), v0);
                        var b2 = Vector3.Dot(Vector3.Cross(v0, v1), v3);
                        var b3 = Vector3.Dot(Vector3.Cross(v2, v1), v0);
                        var sum = b0 + b1 + b2 + b3;

                        if (sum <= 0)
                        {
                            b0 = 0;
                            b1 = Vector3.Dot(Vector3.Cross(v2, v3), n4);
                            b2 = Vector3.Dot(Vector3.Cross(v3, v1), n4);
                            b3 = Vector3.Dot(Vector3.Cross(v1, v2), n4);
                            sum = b1 + b2 + b3;
                        }

                        var inv = 1f / sum;

                        pointA = (b0 * v01 + b1 * v11 + b2 * v21 + b3 * v31) * inv;
                        pointB = (b0 * v02 + b1 * v12 + b2 * v22 + b3 * v32) * inv;

                        // HIT!!!
                        hit = true;
                    }

                    // Find the support point in the direction of the wedge face
                    Vector3 v41, v42;
                    tmp = -n4;
                    a.FindSupport(ref tmp, out v41);
                    b.FindSupport(ref n4, out v42);
                    var v4 = v42 - v41;

                    var delta = Vector3.Dot(v4 - v3, n4);
                    var separation = -Vector3.Dot(v4, n4);

                    // If the boundary is thin enough or the origin is outside the support plane for the newly discovered vertex, then we can terminate
                    if (delta <= CollideEpsilon || separation >= 0 || phase2 > 20)
                    {
                        if (phase2 > 20)
                        {
                            Debug.WriteLine(phase2);
                        }
                        normal = n4;
                        return hit;
                    }

                    // Compute the tetrahedron dividing face (v4,v0,v1)
                    var d1 = Vector3.Dot(Vector3.Cross(v4, v1), v0);

                    // Compute the tetrahedron dividing face (v4,v0,v2)
                    var d2 = Vector3.Dot(Vector3.Cross(v4, v2), v0);

                    // Compute the tetrahedron dividing face (v4,v0,v3)
                    var d3 = Vector3.Dot(Vector3.Cross(v4, v3), v0);

                    if (d1 < 0)
                    {
                        if (d2 < 0)
                        {
                            // Inside d1 & inside d2 ==> eliminate v1
                            v1 = v4;
                            v11 = v41;
                            v12 = v42;
                        }
                        else
                        {
                            // Inside d1 & outside d2 ==> eliminate v3
                            v3 = v4;
                            v31 = v41;
                            v32 = v42;
                        }
                    }
                    else
                    {
                        if (d3 < 0)
                        {
                            // Outside d1 & inside d3 ==> eliminate v2
                            v2 = v4;
                            v21 = v41;
                            v22 = v42;
                        }
                        else
                        {
                            // Outside d1 & outside d3 ==> eliminate v1
                            v1 = v4;
                            v11 = v41;
                            v12 = v42;
                        }
                    }
                }
            }
        }

        static bool doneIt;

        //private static Vector3 FindSupport(ISupportable a, ISupportable b, Vector3 direction)
        //{
        //    return a.FindSupport(direction) - b.FindSupport(-direction);
        //}

        private static void FindSupport(ISupportable a, ISupportable b, Vector3 direction, out Vector3 supportA, out Vector3 support)
        {
            var tmp = -direction;
            a.FindSupport(ref tmp, out supportA);
            b.FindSupport(ref direction, out support);
            support -= supportA;
        }
    }
}
