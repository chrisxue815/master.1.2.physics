﻿using Microsoft.Xna.Framework;

namespace RLGE.Physics.RigidBodyEngine.Collisions.MPR
{
    public interface ISupportable
    {
        void FindSupport(ref Vector3 direction, out Vector3 support);
    }
}
