﻿using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.BroadPhase;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine.Colliders
{
    public class SphereCollider : Collider
    {
        public SphereCollider(RigidBody rigidBody, Vector3 scale)
            : base(rigidBody)
        {
            RigidBody.Scale = scale;
            rigidBody.InverseMass = 0;
            rigidBody.Dynamic = false;

            rigidBody.InverseInertiaTensor = new Matrix(
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 1);
        }

        public SphereCollider(RigidBody rigidBody, Vector3 scale, float mass)
            : base(rigidBody)
        {
            rigidBody.Scale = scale;
            rigidBody.Mass = mass;
            rigidBody.Dynamic = true;

            var wSqr = Width * Width;
            var hSqr = Height * Height;
            var dSqr = Depth * Depth;
            var c = mass / 5;

            rigidBody.InertiaTensor = new Matrix(
                c * (hSqr + dSqr), 0, 0, 0,
                0, c * (wSqr + dSqr), 0, 0,
                0, 0, c * (wSqr * hSqr), 0,
                0, 0, 0, 1);
        }

        public SphereCollider(RigidBody rigidBody)
            : this(rigidBody, new Vector3(1, 1, 1))
        {
        }

        public SphereCollider(RigidBody rigidBody, float width, float height, float depth)
            : this(rigidBody, new Vector3(width, height, depth))
        {
        }

        public SphereCollider(RigidBody rigidBody, float width, float height, float depth, float mass)
            : this(rigidBody, new Vector3(width, height, depth), mass)
        {
        }

        public SphereCollider(RigidBody rigidBody, float scale)
            : this(rigidBody, new Vector3(scale, scale, scale))
        {
        }

        public SphereCollider(RigidBody rigidBody, float scale, float mass)
            : this(rigidBody, new Vector3(scale, scale, scale), mass)
        {
        }

        public override void FindSupport(ref Vector3 direction, out Vector3 support)
        {
            Vector3 localDirection;
            RigidBody.ToLocalNormal(ref direction, out localDirection);
            localDirection *= 0.5f / localDirection.Length();

            RigidBody.ToWorld(ref localDirection, out support);
        }

        public override AABB AABB
        {
            get
            {
                var scale = RigidBody.Scale;
                var maxDimension = Utils.Max(ref scale);
                return new AABB(RigidBody.Position, maxDimension);
            }
        }
    }
}
