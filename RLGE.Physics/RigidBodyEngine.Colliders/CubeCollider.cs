﻿using System;
using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.BroadPhase;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine.Colliders
{
    public class CubeCollider : Collider
    {
        public CubeCollider(RigidBody rigidBody, Vector3 scale)
            : base(rigidBody)
        {
            rigidBody.Scale = scale;
            rigidBody.InverseMass = 0;
            rigidBody.Dynamic = false;

            rigidBody.InverseInertiaTensor = new Matrix(
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 1);
        }

        public CubeCollider(RigidBody rigidBody, Vector3 scale, float mass)
            : base(rigidBody)
        {
            rigidBody.Scale = scale;
            rigidBody.Mass = mass;
            rigidBody.Dynamic = true;

            var wSqr = Width * Width;
            var hSqr = Height * Height;
            var dSqr = Depth * Depth;
            var c = mass / 12;

            rigidBody.InertiaTensor = new Matrix(
                c * (hSqr + dSqr), 0, 0, 0,
                0, c * (wSqr + dSqr), 0, 0,
                0, 0, c * (wSqr * hSqr), 0,
                0, 0, 0, 1);
        }

        public CubeCollider(RigidBody rigidBody)
            : this(rigidBody, new Vector3(1, 1, 1))
        {
        }

        public CubeCollider(RigidBody rigidBody, float width, float height, float depth)
            : this(rigidBody, new Vector3(width, height, depth))
        {
        }

        public CubeCollider(RigidBody rigidBody, float width, float height, float depth, float mass)
            : this(rigidBody, new Vector3(width, height, depth), mass)
        {
        }

        public CubeCollider(RigidBody rigidBody, float scale)
            : this(rigidBody, new Vector3(scale, scale, scale))
        {
        }

        public CubeCollider(RigidBody rigidBody, float scale, float mass)
            : this(rigidBody, new Vector3(scale, scale, scale), mass)
        {
        }

        public override void FindSupport(ref Vector3 direction, out Vector3 support)
        {
            Vector3 localDirection;
            RigidBody.ToLocalNormal(ref direction, out localDirection);

            var x = localDirection.X.Normalize() * 0.5f;
            var y = localDirection.Y.Normalize() * 0.5f;
            var z = localDirection.Z.Normalize() * 0.5f;
            var localSupport = new Vector3(x, y, z);

            RigidBody.ToWorld(ref localSupport, out support);
        }

        public override AABB AABB
        {
            get
            {
                var maxDimension = RigidBody.Scale.Length();
                return new AABB(RigidBody.Position, maxDimension);
            }
        }
    }
}
