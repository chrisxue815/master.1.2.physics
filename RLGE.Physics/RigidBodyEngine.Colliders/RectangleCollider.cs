﻿using System;
using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.BroadPhase;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine.Colliders
{
    public class RectangleCollider : Collider
    {
        public RectangleCollider(RigidBody rigidBody, Vector2 scale)
            : base(rigidBody)
        {
            rigidBody.Scale = new Vector3(scale.X, 1, scale.Y);
            rigidBody.InverseMass = 0;
            rigidBody.Dynamic = false;

            rigidBody.InverseInertiaTensor = new Matrix(
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 1);
        }

        public RectangleCollider(RigidBody rigidBody, Vector2 scale, float mass)
            : base(rigidBody)
        {
            rigidBody.Scale = new Vector3(scale.X, 1, scale.Y);
            rigidBody.Mass = mass;
            rigidBody.Dynamic = true;

            var wSqr = Width * Width;
            var dSqr = Depth * Depth;
            var c = mass / 12;

            rigidBody.InertiaTensor = new Matrix(
                c * dSqr, 0, 0, 0,
                0, c * (wSqr + dSqr), 0, 0,
                0, 0, c * wSqr, 0,
                0, 0, 0, 1);
        }

        public RectangleCollider(RigidBody rigidBody)
            : this(rigidBody, new Vector2(1, 1))
        {
        }

        public RectangleCollider(RigidBody rigidBody, float width, float depth)
            : this(rigidBody, new Vector2(width, depth))
        {
        }

        public RectangleCollider(RigidBody rigidBody, float width, float depth, float mass)
            : this(rigidBody, new Vector2(width, depth), mass)
        {
        }

        public override void FindSupport(ref Vector3 direction, out Vector3 support)
        {
            Vector3 localDirection;
            RigidBody.ToLocalNormal(ref direction, out localDirection);

            var x = localDirection.X.Normalize() * 0.5f;
            var z = localDirection.Z.Normalize() * 0.5f;
            var localSupport = new Vector3(x, 0, z);

            RigidBody.ToWorld(ref localSupport, out support);
        }

        public override AABB AABB
        {
            get
            {
                var scale = RigidBody.Scale;
                var maxDimension = (float)Math.Sqrt(scale.X * scale.X + scale.Y * scale.Y);
                return new AABB(RigidBody.Position, maxDimension);
            }
        }
    }
}
