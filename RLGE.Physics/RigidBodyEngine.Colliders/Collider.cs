using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.BroadPhase;
using RLGE.Physics.RigidBodyEngine.Collisions.MPR;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine.Colliders
{
    public abstract class Collider : ISupportable
    {
        public RigidBody RigidBody { get; protected set; }

        public abstract AABB AABB { get; }

        public float Width
        {
            get { return RigidBody.Scale.X; }
        }

        public float Height
        {
            get { return RigidBody.Scale.Y; }
        }

        public float Depth
        {
            get { return RigidBody.Scale.Z; }
        }

        protected Collider(RigidBody rigidBody)
        {
            RigidBody = rigidBody;
        }

        public abstract void FindSupport(ref Vector3 direction, out Vector3 support);
    }
}
