﻿using System;
using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.BroadPhase;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine.Colliders
{
    public class CylinderCollider : Collider
    {
        public CylinderCollider(RigidBody rigidBody, Vector3 scale)
            : base(rigidBody)
        {
            rigidBody.Scale = scale;
            rigidBody.InverseMass = 0;
            rigidBody.Dynamic = false;

            rigidBody.InverseInertiaTensor = new Matrix(
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 1);
        }

        public CylinderCollider(RigidBody rigidBody, Vector3 scale, float mass)
            : base(rigidBody)
        {
            rigidBody.Scale = scale;
            rigidBody.Mass = mass;
            rigidBody.Dynamic = true;

            var wSqr = Width * Width;
            var hSqr = Height * Height;
            var dSqr = Depth * Depth;
            var c = mass / 12;

            rigidBody.InertiaTensor = new Matrix(
                c * (hSqr + dSqr), 0, 0, 0,
                0, c * (wSqr + dSqr), 0, 0,
                0, 0, c * (wSqr * hSqr), 0,
                0, 0, 0, 1);
        }

        public CylinderCollider(RigidBody rigidBody)
            : this(rigidBody, new Vector3(1, 1, 1))
        {
        }

        public CylinderCollider(RigidBody rigidBody, float width, float height, float depth)
            : this(rigidBody, new Vector3(width, height, depth))
        {
        }

        public CylinderCollider(RigidBody rigidBody, float width, float height, float depth, float mass)
            : this(rigidBody, new Vector3(width, height, depth), mass)
        {
        }

        public CylinderCollider(RigidBody rigidBody, float scale)
            : this(rigidBody, new Vector3(scale, scale, scale))
        {
        }

        public CylinderCollider(RigidBody rigidBody, float scale, float mass)
            : this(rigidBody, new Vector3(scale, scale, scale), mass)
        {
        }

        public override void FindSupport(ref Vector3 direction, out Vector3 support)
        {
            Vector3 localDirection;
            RigidBody.ToLocalNormal(ref direction, out localDirection);

            var ellipseRadius = Utils.SqrtSqSum(localDirection.X, localDirection.Z);

            float x, z;
            var y = localDirection.Y.Normalize() * 0.5f;

            if (ellipseRadius.IsZero())
            {
                x = z = 0;
            }
            else
            {
                x = localDirection.X * 0.5f / ellipseRadius;
                z = localDirection.Z * 0.5f / ellipseRadius;                
            }
            
            var localSupport = new Vector3(x, y, z);

            RigidBody.ToWorld(ref localSupport, out support);
        }

        public override AABB AABB
        {
            get
            {
                var scale = RigidBody.Scale;
                var majorDiameter = Math.Max(scale.X, scale.Z);
                var maxDimension = (float)Math.Sqrt(scale.Y * scale.Y + majorDiameter * majorDiameter);
                return new AABB(RigidBody.Position, maxDimension);
            }
        }
    }
}
