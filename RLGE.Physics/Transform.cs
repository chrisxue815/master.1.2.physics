﻿using Microsoft.Xna.Framework;

namespace RLGE.Physics
{
    public class Transform
    {
        private Matrix positionTransform;
        private Vector3 position;
        public Vector3 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                Matrix.CreateTranslation(ref position, out positionTransform);
                UpdateWorldTransform();
            }
        }

        private Matrix orientationTransform;
        private Quaternion orientation;
        public Quaternion Orientation
        {
            get
            {
                return orientation;
            }
            set
            {
                orientation = value;
                Matrix.CreateFromQuaternion(ref orientation, out orientationTransform);
                UpdateWorldTransform();
            }
        }

        private Matrix scaleTransform;
        private Vector3 scale;
        public Vector3 Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
                Matrix.CreateScale(ref scale, out scaleTransform);
                UpdateWorldTransform();
            }
        }

        public float X
        {
            get { return Position.X; }
        }

        public float Y
        {
            get { return Position.Y; }
        }

        public float Z
        {
            get { return Position.Z; }
        }

        public Matrix WorldTransform;

        public Matrix InverseWorldTransform;

        private void UpdateWorldTransform()
        {
            WorldTransform = scaleTransform * orientationTransform * positionTransform;

            Matrix.Invert(ref WorldTransform, out InverseWorldTransform);
        }
    }
}
