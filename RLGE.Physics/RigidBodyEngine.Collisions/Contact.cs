﻿using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.Colliders;
using RLGE.Physics.RigidBodyEngine.Constraints;

namespace RLGE.Physics.RigidBodyEngine.Collisions
{
    public class Contact : SpaceObject
    {
        public Collider ColliderA;
        public Collider ColliderB;

        public RigidBody A { get { return ColliderA.RigidBody; } }
        public RigidBody B { get { return ColliderB.RigidBody; } }

        /// <summary>
        /// The contact point of A in local space
        /// </summary>
        public Vector3 LocalA;

        /// <summary>
        /// The contact point of B in local space
        /// </summary>
        public Vector3 LocalB;

        public Vector3 Point;

        public Vector3 Normal;

        public long TotalTicks;

        public ContactConstraint Constraint;

        public float DistanceSquared
        {
            get
            {
                Vector3 worldA, worldB;
                A.ToWorld(ref LocalA, out worldA);
                B.ToWorld(ref LocalB, out worldB);
                return (worldB - worldA).LengthSquared();
            }
        }

        public float Distance
        {
            get
            {
                Vector3 worldA, worldB;
                A.ToWorld(ref LocalA, out worldA);
                B.ToWorld(ref LocalB, out worldB);
                return (worldB - worldA).Length();
            }
        }

        public Contact(Space space, Collider colliderA, Collider colliderB,
            ref Vector3 worldA, ref Vector3 worldB, ref Vector3 normal)
            : base(space)
        {
            ColliderA = colliderA;
            ColliderB = colliderB;
            A.ToLocal(ref worldA, out LocalA);
            B.ToLocal(ref worldB, out LocalB);
            Point = 0.5f * (worldA + worldB);
            Normal = normal;
            Constraint = new ContactConstraint(space, A, B, LocalA, LocalB, Normal);
            TotalTicks = space.TotalTicks;
        }

        public void Update(ref Vector3 worldA, ref Vector3 worldB, ref Vector3 normal)
        {
            A.ToLocal(ref worldA, out LocalA);
            B.ToLocal(ref worldB, out LocalB);
            Point = 0.5f * (worldA + worldB);
            Normal = normal;
            TotalTicks = space.TotalTicks;
            Constraint.Update(LocalA, LocalB, Normal);
        }
    }
}
