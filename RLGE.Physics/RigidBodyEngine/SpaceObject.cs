﻿namespace RLGE.Physics.RigidBodyEngine
{
    public abstract class SpaceObject
    {
        protected Space space;

        protected SpaceObject(Space space)
        {
            this.space = space;
        }
    }
}
