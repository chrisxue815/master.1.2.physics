﻿using System.Collections.Generic;
using System.ComponentModel;

namespace RLGE.Physics.RigidBodyEngine
{
    public class TimewarpEvent
    {
        public List<TimewarpState> States;

        public TimewarpEvent()
        {
            States = new List<TimewarpState>();
        }

        public TimewarpEvent(params TimewarpState[] processes)
        {
            States = new List<TimewarpState>(processes);
        }

        public static TimewarpEvent Create(params TimewarpState[] processes)
        {
            return new TimewarpEvent(processes);
        }
    }
}
