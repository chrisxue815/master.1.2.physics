﻿using System;
using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.BroadPhase;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine
{
    public class TimewarpState : IComparable<TimewarpState>
    {
        public TimewarpProcess Process;
        public float Time;
        public Vector3 Position;
        public Quaternion Orientation;
        public Vector3 Velocity;
        public Vector3 AngularVelocity;
        public AABB AABB;

        public TimewarpEvent Event;

        public TimewarpState(TimewarpProcess process)
        {
            Process = process;
        }

        public int CompareTo(TimewarpState other)
        {
            var diff = Time - other.Time;
            return diff < 0 ? -1 : (diff.IsZero() ? 0 : 1);
        }
    }
}
