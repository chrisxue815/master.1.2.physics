using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Effects;
using RLGE.Physics.RigidBodyEngine.BroadPhase;
using RLGE.Physics.RigidBodyEngine.Colliders;
using RLGE.Physics.RigidBodyEngine.Collisions.MPR;
using RLGE.Physics.RigidBodyEngine.Constraints;

namespace RLGE.Physics.RigidBodyEngine
{
    public class Space
    {
        public long ElapsedTicks { get; protected set; }
        public long TotalTicks { get; protected set; }

        public float ElapsedSeconds { get; protected set; }
        public float TotalSeconds { get; protected set; }

        public float ElapsedStepSeconds { get; protected set; }

        public float PreviousElapsedSeconds { get; protected set; }
        public float TimeRatio { get; protected set; }

        public Vector3 GlobalAcceleration { get; set; }
        public List<ParticleEffect> Effects { get; set; }
        private List<TimewarpProcess> Processes { get; set; }
        private List<List<Collider>> ContactGroups { get; set; }

        public readonly SolverManager SolverManager;

        public int BroadPhaseHit { get; private set; }
        public int BroadPhaseMiss { get { return BroadPhaseCount - BroadPhaseHit; } }
        public int BroadPhaseCount { get; private set; }
        public float BroadPhaseHitRate { get { return BroadPhaseCount == 0 ? 0 : (float)BroadPhaseHit / BroadPhaseCount; } }
        public float BroadPhaseMissRate { get { return BroadPhaseCount == 0 ? 0 : (float)BroadPhaseMiss / BroadPhaseCount; } }

        public int NarrowPhaseHit { get; private set; }
        public int NarrowPhaseMiss { get { return NarrowPhaseCount - NarrowPhaseHit; } }
        public int NarrowPhaseCount { get { return BroadPhaseHit; } }
        public float NarrowPhaseHitRate { get { return NarrowPhaseCount == 0 ? 0 : (float)NarrowPhaseHit / NarrowPhaseCount; } }
        public float NarrowPhaseMissRate { get { return NarrowPhaseCount == 0 ? 0 : (float)NarrowPhaseMiss / NarrowPhaseCount; } }

        public Space()
        {
            Effects = new List<ParticleEffect>();
            Processes = new List<TimewarpProcess>();
            ContactGroups = new List<List<Collider>>();
            SolverManager = new SolverManager(this);
        }

        public void Add(Collider collider)
        {
            var process = new TimewarpProcess(collider, TotalSeconds);

            Processes.Add(process);
        }

        public void Add(ParticleEffect particleEffect)
        {
            Effects.Add(particleEffect);
        }

        public void Remove(Collider collider)
        {
            for (var i = Processes.Count - 1; i >= 0; i++)
            {
                if (Processes[i].Collider == collider)
                {
                    Processes.RemoveAt(i);
                }
            }
        }

        public void Remove(List<Collider> colliders)
        {
            foreach (var collider in colliders)
            {
                Remove(collider);
            }
        }

        public void Remove(ParticleEffect particleEffect)
        {
            Effects.Remove(particleEffect);
        }

        public void Clear()
        {
            Processes.Clear();
            Effects.Clear();
            SolverManager.Clear();
        }

        public void Update(long elapsedTicks)
        {
            ElapsedTicks = elapsedTicks;
            TotalTicks += elapsedTicks;

            PreviousElapsedSeconds = ElapsedSeconds;
            ElapsedSeconds = elapsedTicks * 1e-7f;
            TotalSeconds += ElapsedSeconds;
            TimeRatio = ElapsedSeconds / PreviousElapsedSeconds;

            UpdateColliders();
        }

        private void UpdateColliders()
        {
            const int numSteps = 5;
            ElapsedStepSeconds = ElapsedSeconds / numSteps;
            var gvt = float.MinValue; // Global virtual time
            
            while (gvt < TotalSeconds)
            {
                foreach (var process in Processes)
                {
                    if (process.LVT > TotalSeconds) continue;
                    
                    var rigidBody = process.Collider.RigidBody;
                    if (rigidBody.Dynamic) rigidBody.NetForce = GlobalAcceleration * rigidBody.Mass;
                    rigidBody.NetTorque = Vector3.Zero;

                    rigidBody.Update(ElapsedStepSeconds);

                    rigidBody.Velocity *= 0.99f;

                    rigidBody.BroadPhaseCollisionDetected = false;

                    process.SaveState(process.LVT + ElapsedStepSeconds);
                    process.UpdateSweptAABB();
                }
                
                BroadPhaseHit = 0;
                BroadPhaseCount = 0;
                NarrowPhaseHit = 0;

                var worldA = new Vector3();
                var worldB = new Vector3();
                var normal = new Vector3();

                for (var x = 0; x < Processes.Count; x++)
                {
                    var processA = Processes[x];
                    var colliderA = processA.Collider;

                    for (var y = x + 1; y < Processes.Count; y++)
                    {
                        var processB = Processes[y];
                        var colliderB = processB.Collider;

                        BroadPhaseCount++;

                        if (!AABB.FindCollision(processA.SweptAABB, processB.SweptAABB)) continue;

                        BroadPhaseHit++;
                        
                        if (!(colliderA is RectangleCollider))
                        {
                            colliderA.RigidBody.BroadPhaseCollisionDetected = true;
                            colliderB.RigidBody.BroadPhaseCollisionDetected = true;
                        }
                        
                        if (!MPRToolbox.FindCollision(colliderA, colliderB, ref worldA, ref worldB, ref normal)) continue;

                        NarrowPhaseHit++;

                        Vector3 supportA, supportB;
                        var tmp = -normal;
                        colliderA.FindSupport(ref tmp, out supportA);
                        colliderB.FindSupport(ref normal, out supportB);
                        
                        worldA = Vector3.Dot(supportA - worldA, normal) * normal + worldA;
                        worldB = Vector3.Dot(supportB - worldB, normal) * normal + worldB;

                        var solver = SolverManager.GetSolver(colliderA, colliderB);
                        bool newContact;
                        solver.AddContact(colliderA, colliderB, ref worldA, ref worldB, ref normal, out newContact);

                        if (newContact)
                        {
                            //TODO
                            var collisionTime = processA.LVT > processB.LVT ? processA.LVT : processB.LVT;

                            processA.RemoveStatesAfter(collisionTime);
                            processB.RemoveStatesAfter(collisionTime);

                            TimewarpEvent.Create(processA.LatestState, processB.LatestState);
                        }
                    }
                }

                SolverManager.Update();

                var newGVT = float.MaxValue;

                foreach (var process in Processes)
                {
                    if (process.LVT < newGVT) newGVT = process.LVT;
                }

                gvt = newGVT;

                foreach (var process in Processes)
                {
                    process.RemoveStatesBefore(gvt);
                }
            }
        }
    }
}
