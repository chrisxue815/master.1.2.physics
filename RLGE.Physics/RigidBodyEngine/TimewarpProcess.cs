﻿using RLGE.Physics.RigidBodyEngine.BroadPhase;
using RLGE.Physics.RigidBodyEngine.Colliders;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine
{
    public class TimewarpProcess
    {
        public Collider Collider;
        public SortedQueue<TimewarpState> States;
        public AABB SweptAABB { get; private set; }

        public TimewarpState EarliestState { get { return States.First; } }
        public TimewarpState LatestState { get { return States.Last; } }

        /// <summary>
        /// Local virtual time
        /// </summary>
        public float LVT { get { return States.Last.Time; } }

        public TimewarpProcess(Collider collider, float time)
        {
            Collider = collider;
            States = new SortedQueue<TimewarpState>();
            SaveState(time);
        }

        /// <summary>
        /// Rollback
        /// </summary>
        /// <param name="time"></param>
        public void RemoveStatesAfter(float time)
        {
            for (var i = States.Count - 1; i >= 0; i--)
            {
                var state = States[i];

                if (state.Time <= time)
                {
                    LoadState(state);
                    break;
                }

                States.RemoveAt(i);

                if (state.Event != null)
                {
                    foreach (var eventState in state.Event.States)
                    {
                        if (eventState != state)
                        {
                            eventState.Process.RemoveStatesAfter(state.Time);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Shrink
        /// </summary>
        /// <param name="time"></param>
        public void RemoveStatesBefore(float time)
        {
            while (States.Count > 0)
            {
                var state = States[0];

                if (state.Time >= time) break;

                States.RemoveAt(0);
            }
        }

        public void SaveState(float time)
        {
            var state = new TimewarpState(this)
            {
                Time = time,
                Position = Collider.RigidBody.Position,
                Orientation = Collider.RigidBody.Orientation,
                Velocity = Collider.RigidBody.Velocity,
                AngularVelocity = Collider.RigidBody.AngularVelocity,
                AABB = Collider.AABB
            };

            States.Add(state);
        }

        public void LoadState(TimewarpState state)
        {

        }

        public void UpdateSweptAABB()
        {
            SweptAABB = States.First.AABB;
            for (var i = 1; i < States.Count; i++)
            {
                SweptAABB.Dilate(States[i].AABB);
            }
        }
    }
}
