﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Effects;
using RLGE.Physics.ParticleEffectEngine.Modifiers;

namespace RLGE.Physics.ParticleEffectEngine.Emitters
{
    public class Emitter
    {
        protected ParticleEffect ParticleEffect { get; set; }

        public Vector3 Position { get { return ParticleEffect.Position; } }
        public List<Modifier> Modifiers { get; set; }
        public float MinimumTriggerPeriod { get; set; }
        public float ReleaseSpeed { get; set; }
        public Vector3 ReleaseDiffuseColor { get; set; }

        private float TriggerTimeCount { get; set; }  //TODO: rename

        public Emitter(ParticleEffect particleEffect)
        {
            ParticleEffect = particleEffect;
            Modifiers = new List<Modifier>();
        }

        public virtual List<RigidBody> Update(float elapsedSeconds)
        {
            TriggerTimeCount += elapsedSeconds;

            if (TriggerTimeCount > MinimumTriggerPeriod)
            {
                TriggerTimeCount -= MinimumTriggerPeriod;
                return GenerateParticles();
            }

            return null;
        }

        public virtual List<RigidBody> GenerateParticles()
        {
            return null;
        }
    }
}
