﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Effects;

namespace RLGE.Physics.ParticleEffectEngine.Emitters
{
    public class ConeEmitter : Emitter
    {
        public Vector3 Direction { get; set; }
        public float Angle { get; set; }
        private Random Random { get; set; }

        public ConeEmitter(ParticleEffect particleEffect)
            : base(particleEffect)
        {
            Random = new Random();
        }

        public override List<RigidBody> GenerateParticles()
        {
            var particles = new List<RigidBody>();

            var q = Quaternion.CreateFromAxisAngle(Vector3.Backward, Angle / 2);
            var a = Vector3.Transform(Direction, q);
            var b = (float)Random.NextDouble() * MathHelper.TwoPi;
            q = Quaternion.CreateFromAxisAngle(Direction, b);
            var d = Vector3.Transform(a, q);

            var velocity = d * ReleaseSpeed;

            var particle = new RigidBody()
            {
                Position = Position,
                Velocity = velocity,
                DiffuseColor = ReleaseDiffuseColor,
                Modifiers = Modifiers,
            };

            particles.Add(particle);

            return particles;
        }
    }
}
