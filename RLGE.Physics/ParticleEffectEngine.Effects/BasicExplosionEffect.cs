﻿using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Emitters;
using RLGE.Physics.ParticleEffectEngine.Modifiers;

namespace RLGE.Physics.ParticleEffectEngine.Effects
{
    public class BasicExplosionEffect : ParticleEffect
    {
        public BasicExplosionEffect(Vector3 position)
        {
            Position = position;

            var pointEmitter = new PointEmitter(this)
            {
                MinimumTriggerPeriod = 0.01f,
                ReleaseSpeed = 5,
                ReleaseDiffuseColor = new Vector3(1, 0, 0)
            };

            pointEmitter.Modifiers.Add(ConstantForceModifier.Gravity);
            pointEmitter.Modifiers.Add(new RandomSphereForceModifier(new Vector3(1, 0.5f, 0)));
            pointEmitter.Modifiers.Add(new DiffuseColorModifier());

            Emitters.Add(pointEmitter);
        }
    }
}
