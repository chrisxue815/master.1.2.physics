﻿using Microsoft.Xna.Framework;

namespace RLGE.Physics.ParticleEffectEngine.Modifiers
{
    public class DiffuseColorModifier : Modifier
    {
        public override void Update(float elapsedSeconds, RigidBody particle)
        {
            if (particle.DiffuseColor.Y < 1)
            {
                particle.DiffuseColor += elapsedSeconds * new Vector3(-0.2f, 0.2f, 0);
            }
            else if (particle.DiffuseColor.Y < 1)
            {
                //particle.DiffuseColor += elapsedSeconds * new Vector3(-0.5f, 0.5f, 0);
            }
        }
    }
}
