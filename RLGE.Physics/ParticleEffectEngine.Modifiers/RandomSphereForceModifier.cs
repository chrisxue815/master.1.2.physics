﻿using System;
using Microsoft.Xna.Framework;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.ParticleEffectEngine.Modifiers
{
    public class RandomSphereForceModifier : Modifier
    {
        public Vector3 Force { get; set; }
        private Random Random { get; set; }

        public RandomSphereForceModifier(Vector3 force)
        {
            Force = force;
            Random = new Random();
        }

        public override void Update(float elapsedSeconds, RigidBody particle)
        {
            Force += new Vector3(RandomExt.NextFloat(0.1f), RandomExt.NextFloat(0.1f), RandomExt.NextFloat(0.1f));
            particle.NetForce += Force;
        }
    }
}
