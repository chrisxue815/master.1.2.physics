﻿using Microsoft.Xna.Framework;

namespace RLGE.Physics.RigidBodyEngine.Constraints
{
    public abstract class Constraint : SpaceObject
    {
        protected Constraint(Space space)
            : base(space)
        {
        }

        public abstract void PrepareForIteration();

        public abstract void Iterate();
    }
}
