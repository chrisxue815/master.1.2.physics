﻿namespace RLGE.Physics.RigidBodyEngine.Constraints
{
    public struct SolverKey
    {
        public RigidBody A;
        public RigidBody B;

        public SolverKey(RigidBody a, RigidBody b)
        {
            if (a < b)
            {
                A = a;
                B = b;
            }
            else
            {
                A = b;
                B = a;
            }
        }
    }
}
