﻿using System;
using Microsoft.Xna.Framework;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine.Constraints
{
    public class ContactConstraint : Constraint
    {
        private readonly RigidBody a;
        private readonly RigidBody b;

        /// <summary>
        /// The contact point of A in local space
        /// </summary>
        private Vector3 localA;

        /// <summary>
        /// The contact point of B in local space
        /// </summary>
        private Vector3 localB;

        private float positionError;
        private Vector3 normal;

        private Vector3 invMomentA;
        private Vector3 invMomentB;

        private float effectiveMass;

        private float beta;
        private float cachedMomentum;
        private Vector3 cachedAngularMomentum;

        public ContactConstraint(Space space, RigidBody a, RigidBody b, Vector3 localA, Vector3 localB, Vector3 normal)
            : base(space)
        {
            this.a = a;
            this.b = b;
            this.localA = localA;
            this.localB = localB;
            this.normal = normal;
            beta = 1;
            cachedMomentum = 0;
            cachedAngularMomentum = new Vector3();
        }

        public override void PrepareForIteration()
        {
            Vector3 ra, rb;
            a.ToWorldNormal(ref localA, out ra);
            b.ToWorldNormal(ref localB, out rb);

            var pa = a.Position + ra;
            var pb = b.Position + rb;

            beta = 0.5f / space.ElapsedStepSeconds;
            positionError = Vector3.Dot(beta * (pb - pa), normal);
            positionError -= 1f;

            invMomentA = Vector3.TransformNormal(Vector3.Cross(ra, normal), a.InverseWorldInertiaTensor);
            invMomentB = Vector3.TransformNormal(Vector3.Cross(rb, normal), b.InverseWorldInertiaTensor);

            var term1 = a.InverseMass;
            var term2 = b.InverseMass;
            var term3 = Vector3.Dot(normal, Vector3.Cross(invMomentA, ra) + Vector3.Cross(invMomentB, rb));

            effectiveMass = 1f / (term1 + term2 + term3);

            cachedMomentum *= space.TimeRatio;
            cachedAngularMomentum *= space.TimeRatio;

            a.Velocity -= cachedMomentum * normal * a.InverseMass;
            b.Velocity += cachedMomentum * normal * b.InverseMass;
            a.AngularVelocity -= cachedMomentum * invMomentA;
            b.AngularVelocity += cachedMomentum * invMomentB;

            cachedAngularMomentum = new Vector3();
        }

        public override void Iterate()
        {
            Vector3 ra, rb;
            a.ToWorldNormal(ref localA, out ra);
            b.ToWorldNormal(ref localB, out rb);

            var padot = a.Velocity + Vector3.Cross(a.AngularVelocity, ra);
            var pbdot = b.Velocity + Vector3.Cross(b.AngularVelocity, rb);
            var relativeVelocity = pbdot - padot;

            var velocityError = Vector3.Dot(relativeVelocity, normal);

            var deltaVelocity = -velocityError - positionError;

            var momentumPacket = deltaVelocity * effectiveMass;

            momentumPacket = Math.Min(momentumPacket, -cachedMomentum);

            var momentumPacketWithDir = momentumPacket * normal;

            a.Velocity -= momentumPacketWithDir * a.InverseMass;
            b.Velocity += momentumPacketWithDir * b.InverseMass;
            a.AngularVelocity -= momentumPacket * invMomentA;
            b.AngularVelocity += momentumPacket * invMomentB;

            cachedMomentum += momentumPacket;

            const bool friction = true;

            if (friction)
            {
                padot = a.Velocity + Vector3.Cross(a.AngularVelocity, ra);
                pbdot = b.Velocity + Vector3.Cross(b.AngularVelocity, rb);

                relativeVelocity = pbdot - padot;

                var angularVelocityDir = relativeVelocity - Vector3.Dot(relativeVelocity, normal) * normal;
                if (angularVelocityDir.IsZero()) return;
                angularVelocityDir.Normalize();

                var angularVelocityError = Vector3.Dot(relativeVelocity, angularVelocityDir);

                invMomentA = Vector3.TransformNormal(Vector3.Cross(ra, angularVelocityDir), a.InverseWorldInertiaTensor);
                invMomentB = Vector3.TransformNormal(Vector3.Cross(rb, angularVelocityDir), b.InverseWorldInertiaTensor);

                var term1 = a.InverseMass;
                var term2 = b.InverseMass;
                var term3 = Vector3.Dot(angularVelocityDir, Vector3.Cross(invMomentA, ra) + Vector3.Cross(invMomentB, rb));

                effectiveMass = 1f / (term1 + term2 + term3);

                var angularDeltaVelocity = -angularVelocityError;
                var angularMomentumPacket = angularDeltaVelocity * effectiveMass;
                angularMomentumPacket = Math.Max(angularMomentumPacket, cachedMomentum * 0.5f);

                var angularMomentumPacketWithDir = angularMomentumPacket * angularVelocityDir;

                a.Velocity -= angularMomentumPacketWithDir * a.InverseMass;
                b.Velocity += angularMomentumPacketWithDir * b.InverseMass;
                a.AngularVelocity -= angularMomentumPacket * invMomentA;
                b.AngularVelocity += angularMomentumPacket * invMomentB;

                cachedAngularMomentum = angularMomentumPacketWithDir;
            }
        }

        public void Update(Vector3 localA, Vector3 localB, Vector3 normal)
        {
            this.localA = localA;
            this.localB = localB;
            this.normal = normal;
        }
    }
}
