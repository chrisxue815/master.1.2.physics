﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.Colliders;
using RLGE.Physics.RigidBodyEngine.Collisions;

namespace RLGE.Physics.RigidBodyEngine.Constraints
{
    public class SolverManager : SpaceObject, IEnumerable<Solver>
    {
        public int Iteration;

        private readonly Dictionary<SolverKey, Solver> solvers;

        public Solver GetSolver(RigidBody a, RigidBody b)
        {
            var key = new SolverKey(a, b);

            Solver solver;
            solvers.TryGetValue(key, out solver);

            if (solver == null)
            {
                solver = new Solver(space, this);
                solvers[key] = solver;
            }

            return solver;
        }

        public Solver GetSolver(Collider a, Collider b)
        {
            return GetSolver(a.RigidBody, b.RigidBody);
        }

        public SolverManager(Space space)
            : base(space)
        {
            solvers = new Dictionary<SolverKey, Solver>();
        }

        public void AddContact(Collider a, Collider b, ref Vector3 worldA, ref Vector3 worldB, ref Vector3 normal, out bool newContact)
        {
            var solver = GetSolver(a.RigidBody, b.RigidBody);
            solver.AddContact(a, b, ref worldA, ref worldB, ref normal, out newContact);
        }

        public void Clear()
        {
            solvers.Clear();
        }

        public void Update()
        {
            for (Iteration = 0; Iteration < 2; Iteration++)
            {
                foreach (var solver in solvers.Values)
                {
                    solver.Update();
                }
            }
        }

        public IEnumerator<Solver> GetEnumerator()
        {
            return solvers.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
