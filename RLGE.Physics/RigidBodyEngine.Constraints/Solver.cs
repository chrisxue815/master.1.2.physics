﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.RigidBodyEngine.Colliders;
using RLGE.Physics.RigidBodyEngine.Collisions;

namespace RLGE.Physics.RigidBodyEngine.Constraints
{
    public class Solver : SpaceObject, IEnumerable<Contact>
    {
        private readonly SolverManager manager;
        private readonly List<Contact> contacts = new List<Contact>();

        public Solver(Space space, SolverManager manager)
            : base(space)
        {
            this.manager = manager;
        }

        public void AddContact(Collider a, Collider b, ref Vector3 worldA, ref Vector3 worldB, ref Vector3 normal, out bool newContact)
        {
            var point = 0.5f * (worldA + worldB);

            foreach (var contact in contacts)
            {
                if ((contact.Point - point).LengthSquared() < 1)
                {
                    contact.Update(ref worldA, ref worldB, ref normal);
                    newContact = false;
                    return;
                }
            }

            contacts.Add(new Contact(space, a, b, ref worldA, ref worldB, ref normal));
            newContact = true;
        }

        public void Add(Contact contact)
        {
            contacts.Add(contact);
        }

        public void Update()
        {
            for (var i = contacts.Count - 1; i >= 0; i--)
            {
                var contact = contacts[i];

                if (contact.DistanceSquared > 0.04f && contact.TotalTicks != space.TotalTicks)
                {
                    contacts.RemoveAt(i);
                    continue;
                }

                if (manager.Iteration == 0)
                {
                    contact.Constraint.PrepareForIteration();
                }
                else
                {
                    contact.Constraint.Iterate();
                }
            }
        }

        public IEnumerator<Contact> GetEnumerator()
        {
            return contacts.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
