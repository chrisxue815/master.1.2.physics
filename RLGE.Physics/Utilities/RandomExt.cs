﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;

namespace RLGE.Physics.Utilities
{
    public class RandomExt
    {
        private static Random Random { get; set; }

        static RandomExt()
        {
            Random = new Random();
        }

        /// <summary>
        /// [0, 1)
        /// </summary>
        /// <returns></returns>
        public static float NextPosFloat()
        {
            return (float)Random.NextDouble();
        }

        /// <summary>
        /// [0, max)
        /// </summary>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float NextPosFloat(float max)
        {
            return (float)(Random.NextDouble() * max);
        }

        /// <summary>
        /// [min, max)
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float NextFloat(float min, float max)
        {
            var range = max - min;
            var rand = (float)(Random.NextDouble() * range);
            return rand + min;
        }

        /// <summary>
        /// [-1, 1)
        /// </summary>
        /// <returns></returns>
        public static float NextFloat()
        {
            return (float)(Random.NextDouble() * 2 - 1);
        }

        /// <summary>
        /// [-max, max)
        /// </summary>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float NextFloat(float max)
        {
            return (float)(Random.NextDouble() * 2 * max - max);
        }

        /// <summary>
        /// [0, 1) for each dimension
        /// </summary>
        /// <returns></returns>
        public static Vector3 NextPosVector3()
        {
            return new Vector3(NextPosFloat(), NextPosFloat(), NextPosFloat());
        }

        /// <summary>
        /// [0, max) for each dimension
        /// </summary>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Vector3 NextPosVector3(float max)
        {
            return new Vector3(NextPosFloat(max), NextPosFloat(max), NextPosFloat(max));
        }

        /// <summary>
        /// [-1, 1) for each dimension
        /// </summary>
        /// <returns></returns>
        public static Vector3 NextVector3()
        {
            return new Vector3(NextFloat(), NextFloat(), NextFloat());
        }

        /// <summary>
        /// [-max, max) for each dimension
        /// </summary>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Vector3 NextVector3(float max)
        {
            return new Vector3(NextFloat(max), NextFloat(max), NextFloat(max));
        }

        /// <summary>
        /// [min, max) for each dimension
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Vector3 NextVector3(float min, float max)
        {
            return new Vector3(NextFloat(min, max), NextFloat(min, max), NextFloat(min, max));
        }


        public static bool NextBool()
        {
            return Random.Next(2) == 1;
        }

        public static bool NextBool(float possibility)
        {
            return Random.NextDouble() < possibility;
        }

        /// <summary>
        /// [0, ~)
        /// </summary>
        /// <returns></returns>
        public static int NextInt()
        {
            return Random.Next();
        }

        /// <summary>
        /// [0, max)
        /// </summary>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int NextInt(int max)
        {
            return Random.Next(max);
        }

        /// <summary>
        /// [min, max)
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int NextInt(int min, int max)
        {
            return Random.Next(min, max);
        }

        public static int NextInt(float[] possibilities)
        {
            var rand = NextPosFloat();
            var accumulator = 0.0f;
            var i = 0;

            for (; i < possibilities.Count(); i++)
            {
                accumulator += possibilities[i];
                if (accumulator >= rand)
                {
                    break;
                }
            }

            return i;
        }

        public static T NextEnum<T>(T t, float[] possibilities)
        {
            var rand = NextInt(possibilities);
            return (T)Enum.GetValues(typeof(T)).GetValue(rand);
        }
    }
}
