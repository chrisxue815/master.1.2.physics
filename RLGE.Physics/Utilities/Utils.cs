﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;

namespace RLGE.Physics.Utilities
{
    public static class Utils
    {
        public const float PositiveZero = 1f / 16384f;
        public const float NegativeZero = -PositiveZero;

        public static bool IsZero(this Vector3 v)
        {
            return v.X.IsZero() && v.Y.IsZero() && v.Z.IsZero();
        }

        public static bool IsZero(this float f)
        {
            return f > NegativeZero && f < PositiveZero;
        }

        public static float Normalize(this float f)
        {
            if (f < NegativeZero) return -1;
            else if (f < NegativeZero) return 0;
            else return 1;
        }

        public static void Swap<T>(ref T a, ref T b)
        {
            var c = a;
            a = b;
            b = c;
        }

        public static T Max<T>(T t1, T t2, T t3) where T : IComparable<T>
        {
            return t1.CompareTo(t2) > 0 ? (t1.CompareTo(t3) > 0 ? t1 : t3) : (t2.CompareTo(t3) > 0 ? t2 : t3);
        }

        public static float SqSum(params float[] numbers)
        {
            return numbers.Sum(number => number * number);
        }

        public static float SqrtSqSum(params float[] numbers)
        {
            return (float)Math.Sqrt(numbers.Sum(number => number * number));
        }

        public static void Max(ref Vector3 a, ref Vector3 b, out Vector3 result)
        {
            result.X = a.X > b.X ? a.X : b.X;
            result.Y = a.Y > b.Y ? a.Y : b.Y;
            result.Z = a.Z > b.Z ? a.Z : b.Z;
        }

        public static void Min(ref Vector3 a, ref Vector3 b, out Vector3 result)
        {
            result.X = a.X < b.X ? a.X : b.X;
            result.Y = a.Y < b.Y ? a.Y : b.Y;
            result.Z = a.Z < b.Z ? a.Z : b.Z;
        }

        public static float Max(ref Vector3 a)
        {
            return a.X > a.Y ? (a.X > a.Z ? a.X : a.Z) : (a.Y > a.Z ? a.Y : a.Z);
        }
    }
}
