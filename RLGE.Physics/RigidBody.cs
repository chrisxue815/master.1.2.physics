﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using RLGE.Physics.ParticleEffectEngine.Modifiers;

namespace RLGE.Physics
{
    public class RigidBody : Transform, IComparable<RigidBody>
    {
        protected static int nextId = 0;
        public int Id { get; protected set; }

        public float InverseMass;
        public float Mass
        {
            get { return 1 / InverseMass; }
            set { InverseMass = 1 / value; }
        }

        public bool Dynamic;
        public bool Kinematic
        {
            get { return !Dynamic; }
            set { Dynamic = !value; }
        }

        public Matrix InverseInertiaTensor;
        public Matrix InertiaTensor
        {
            get { return Matrix.Invert(InverseInertiaTensor); }
            set { InverseInertiaTensor = Matrix.Invert(value); }
        }

        public Matrix InverseWorldInertiaTensor
        {
            get
            {
                var orientationMatrix = Matrix.CreateFromQuaternion(Orientation);
                // the inverse of an orientation matrix is its transpose
                return Matrix.Transpose(orientationMatrix) * InverseInertiaTensor * orientationMatrix;
            }
        }

        public Vector3 Velocity;
        public Vector3 AngularVelocity;

        public Vector3 NetForce;
        public Vector3 NetTorque;

        public Vector3 PreviousPosition;
        public Quaternion PreviousOrientation;

        public Vector3 InitialForward;
        public Vector3 InitialRight;
        public Vector3 InitialUp;

        public Vector3 Direction
        {
            get { return Vector3.Normalize(Vector3.Transform(InitialForward, Orientation)); }
            set
            {
                var angle = Vector3.Dot(InitialForward, value);
                var axis = Vector3.Normalize(Vector3.Cross(InitialForward, value));
                Orientation = Quaternion.CreateFromAxisAngle(axis, angle);
            }
        }

        public Vector3 DiffuseColor;
        public float Alpha;
        public List<Modifier> Modifiers;

        public bool BroadPhaseCollisionDetected;

        public RigidBody()
        {
            Id = nextId++;
            Modifiers = new List<Modifier>();
            Orientation = Quaternion.Identity;
            InitialForward = Vector3.Forward;
            Alpha = 1;
        }

        public virtual void Update(float elapsedSeconds)
        {
            foreach (var modifier in Modifiers)
            {
                modifier.Update(elapsedSeconds, this);
            }

            PreviousPosition = Position;
            PreviousOrientation = Orientation;

            var linearAcceleration = NetForce * InverseMass;
            Velocity += linearAcceleration * elapsedSeconds;
            Position += Velocity * elapsedSeconds;

            var angularAcceleration = Vector3.TransformNormal(NetTorque, InverseWorldInertiaTensor);
            AngularVelocity += angularAcceleration * elapsedSeconds;

            if (AngularVelocity.Length() > 0)
            {
                var axis = Vector3.Normalize(AngularVelocity);
                var angle = AngularVelocity.Length() * elapsedSeconds;
                var rotation = Quaternion.CreateFromAxisAngle(axis, angle);
                Orientation *= rotation;
            }
        }

        public void Revert()
        {
            Position = PreviousPosition;
            Orientation = PreviousOrientation;
        }

        public void ToWorld(ref Vector3 v, out Vector3 world)
        {
            var t = WorldTransform;
            Vector3.Transform(ref v, ref t, out world);
        }

        public void ToLocal(ref Vector3 v, out Vector3 local)
        {
            var t = InverseWorldTransform;
            Vector3.Transform(ref v, ref t, out local);
        }

        public void ToWorldNormal(ref Vector3 v, out Vector3 worldNormal)
        {
            var t = WorldTransform;
            Vector3.TransformNormal(ref v, ref t, out worldNormal);
        }

        public void ToLocalNormal(ref Vector3 v, out Vector3 localNormal)
        {
            var t = InverseWorldTransform;
            Vector3.TransformNormal(ref v, ref t, out localNormal);
        }

        public int CompareTo(RigidBody other)
        {
            return Id - other.Id;
        }

        public static bool operator ==(RigidBody a, RigidBody b)
        {
            return (a == null && b == null) ||
                (a != null && b != null && a.Id == b.Id);
        }

        public static bool operator !=(RigidBody a, RigidBody b)
        {
            return !(a == b);
        }

        public static bool operator <(RigidBody a, RigidBody b)
        {
            return a.Id < b.Id;
        }

        public static bool operator >(RigidBody a, RigidBody b)
        {
            return a.Id > b.Id;
        }

        public static bool operator <=(RigidBody a, RigidBody b)
        {
            return a.Id <= b.Id;
        }

        public static bool operator >=(RigidBody a, RigidBody b)
        {
            return a.Id >= b.Id;
        }

        protected bool Equals(RigidBody other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((RigidBody)obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}
