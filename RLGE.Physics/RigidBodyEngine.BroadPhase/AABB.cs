﻿using Microsoft.Xna.Framework;
using RLGE.Physics.Utilities;

namespace RLGE.Physics.RigidBodyEngine.BroadPhase
{
    public class AABB
    {
        public Vector3 Position { get; private set; }
        public Vector3 HalfScale;

        public Vector3 Scale
        {
            get { return HalfScale * 2; }
            set { HalfScale = value / 2; }
        }

        public float HalfWidth
        {
            get { return HalfScale.X; }
            set { HalfScale.X = value; }
        }

        public float HalfHeight
        {
            get { return HalfScale.Y; }
            set { HalfScale.Y = value; }
        }

        public float HalfDepth
        {
            get { return HalfScale.Z; }
            set { HalfScale.Z = value; }
        }

        public float Width
        {
            get { return HalfScale.X * 2; }
            set { HalfScale.X = value / 2; }
        }

        public float Height
        {
            get { return HalfScale.Y * 2; }
            set { HalfScale.Y = value / 2; }
        }

        public float Depth
        {
            get { return HalfScale.Z * 2; }
            set { HalfScale.Z = value / 2; }
        }

        public float X
        {
            get { return Position.X; }
        }

        public float Y
        {
            get { return Position.Y; }
        }

        public float Z
        {
            get { return Position.Z; }
        }

        public Vector3 Max
        {
            get { return Position + HalfScale; }
        }

        public Vector3 Min
        {
            get { return Position - HalfScale; }
        }

        public AABB(Vector3 position, Vector3 scale)
        {
            Position = position;
            Scale = scale;
        }

        public AABB(Vector3 position, float scale)
            : this(position, new Vector3(scale))
        {
        }

        public static AABB FromBounds(ref Vector3 max, ref Vector3 min)
        {
            var scale = max - min;
            var position = min + scale / 2;
            return new AABB(position, scale);
        }

        public static AABB Dilate(AABB a, AABB b)
        {
            Vector3 max, min;
            var aMax = a.Max;
            var bMax = b.Max;
            var aMin = a.Min;
            var bMin = b.Min;
            Utils.Max(ref aMax, ref bMax, out max);
            Utils.Min(ref aMin, ref bMin, out min);
            return FromBounds(ref max, ref min);
        }

        public void Dilate(AABB b)
        {
            Vector3 max, min;
            var aMax = Max;
            var bMax = b.Max;
            var aMin = Min;
            var bMin = b.Min;
            Utils.Max(ref aMax, ref bMax, out max);
            Utils.Min(ref aMin, ref bMin, out min);
            Scale = max - min;
            Position = min + HalfScale;
        }

        public static bool FindCollision(AABB a, AABB b)
        {
            if (a.X < b.X)
            {
                if (a.X + a.HalfWidth < b.X - b.HalfWidth) return false;
            }
            else
            {
                if (b.X + b.HalfWidth < a.X - a.HalfWidth) return false;
            }

            if (a.Y < b.Y)
            {
                if (a.Y + a.HalfHeight < b.Y - b.HalfHeight) return false;
            }
            else
            {
                if (b.Y + b.HalfHeight < a.Y - a.HalfHeight) return false;
            }

            if (a.Z < b.Z)
            {
                if (a.Z + a.HalfDepth < b.Z - b.HalfDepth) return false;
            }
            else
            {
                if (b.Z + b.HalfDepth < a.Z - a.HalfDepth) return false;
            }

            return true;
        }
    }
}
